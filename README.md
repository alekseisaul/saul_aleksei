My name is Aleksey Saul. I'm a Java student - intern at Tlalim MASA Project. This is my repository for learning tasks.

Folders:

/src/simple_bash_interpreter - a simple bash-interpreter project;

/src/cron - a Java-wrapper for Cron util;

/src/rock_paper_scissors - a simple rock-paper-scissors console game;

/src/Sandbox - a sandbox for tests;

