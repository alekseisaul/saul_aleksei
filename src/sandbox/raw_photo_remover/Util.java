package sandbox.raw_photo_remover;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Util {
    private static final String WRONG_INPUT = "Wrong input!!!";
    private static final String GREETING = "This is a raw file remover Util. The util cleans up selected folder from unused Raw-photos;";
    private static final String INPUT_REQUEST = "Input a path to a folder you'd like to process";

    private static String readFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            System.err.println(WRONG_INPUT);
        }

        return input;
    }

    static void printGreeting() {
        System.out.println(GREETING);
    }

    static void printRequestForInput() {
        System.out.println(INPUT_REQUEST);
    }

    static File getRoot() throws rawPhotoRemoverException {
        String path = Util.readFromConsole();
        File root = new File(path);
        if(root.isDirectory()){
            return root;

        } else {
            throw new rawPhotoRemoverException("Input path is not a folder!");
        }
    }
}
