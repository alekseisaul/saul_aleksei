package sandbox.raw_photo_remover;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import raw_photo_remover.Controller;

import java.io.*;
import java.util.*;


public class rawPhotoRemover {
    private static final String CHECKED_MARK = "checked_mark";
    private static final String RAW_EXTENSION = ".NEF";
    private static final String PSD_EXTENSION = ".psd";
    private static final String JPG_EXTENSION = ".jpg";
    private static final String XMP_EXTENSION = ".xmp";

    private static Set<File> rawFiles;
    private static Set<File> jpgFiles;
    private static Set<File> psdFiles;
    private static Set<File> xmpFiles;
    private static File root;
    private static Set<File> rawToDelete;


    public static void main(String[] args) throws rawPhotoRemoverException {
        Util.printGreeting();
        Util.printRequestForInput();

        root = Util.getRoot();

        File checked_mark = new File(root, CHECKED_MARK);
        if (!checked_mark.exists()) {
            File[] allFiles = root.listFiles();
            rawFiles = new HashSet<>(
                    FileUtils.listFiles(root, new SuffixFileFilter(RAW_EXTENSION),
                            TrueFileFilter.INSTANCE));
            psdFiles = new HashSet<>(
                    FileUtils.listFiles(getFolder(allFiles, "psd"),
                            new SuffixFileFilter(PSD_EXTENSION), TrueFileFilter.INSTANCE));
            jpgFiles = new HashSet<>(
                    FileUtils.listFiles(getFolder(allFiles, "jpg"),
                            new SuffixFileFilter(JPG_EXTENSION), TrueFileFilter.INSTANCE));
            xmpFiles = new HashSet<>(
                    FileUtils.listFiles(root, new SuffixFileFilter(XMP_EXTENSION),
                            TrueFileFilter.INSTANCE));
            checkIfRawNeedToBeDeleted();
        } else {
            System.out.println("The folder has already reorganized.");
        }

        System.out.println(rawFiles);
        System.out.println(rawToDelete);
        removePhotos(rawToDelete);
        File mark = new File(root, CHECKED_MARK);
        try(BufferedWriter bwr = new BufferedWriter(new FileWriter(mark))){
            bwr.write(new Date().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getFolder(File[] allFiles, String folderName) {
        File folder;
        for (File tmp : allFiles) {
            if (tmp.getName().toLowerCase().equals(folderName) && tmp.exists()) {
                System.out.println(String.format("Folder %s has already exist", folderName));
                return tmp;}}
        folder = new File(root, folderName);
        try {
            folder.mkdir();
            System.out.println(String.format("Folder %s created", folderName));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return folder;
    }

    private static void removePhotos(Set<File> rawToDelete){
        for(File raw : rawToDelete){
            raw.delete();
        }

    }

    private static void checkIfRawNeedToBeDeleted() {
        Set<String> psdFilesNames = new HashSet<>();
        Set<String> jpgFilesNames = new HashSet<>();
        Set<String> xmpFileNames = new HashSet<>();
        rawToDelete = new HashSet<>();

        for(File psd : psdFiles){
            psdFilesNames.add(psd.getName());
        }

        for(File jpg : jpgFiles){
            jpgFilesNames.add(jpg.getName());
        }

        for(File xmp : xmpFiles){
            xmpFileNames.add(xmp.getName());
        }

        for(File tmp : rawFiles){
            if(!(psdFilesNames.contains(tmp.getName().replace(RAW_EXTENSION, PSD_EXTENSION)) ||
                    jpgFilesNames.contains(tmp.getName().replace(RAW_EXTENSION, JPG_EXTENSION)) ||
                    xmpFileNames.contains(tmp.getName().replace(RAW_EXTENSION, XMP_EXTENSION)))){
                rawToDelete.add(tmp);
            }
        }
    }
}
