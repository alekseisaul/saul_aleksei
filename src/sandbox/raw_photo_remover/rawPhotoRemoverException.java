package sandbox.raw_photo_remover;

class rawPhotoRemoverException extends Throwable {
    rawPhotoRemoverException(String s) {
        super(s);
    }
}
