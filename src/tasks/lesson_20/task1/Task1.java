package tasks.lesson_20.task1;

import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        PythagorasChecker checker = new PythagorasChecker(100);
        checker.find();
        List<Triangle> triples = checker.find();
        for(Triangle tmp : triples){
            System.out.println(tmp);
        }
        System.out.println(checker.gcd(27, 9));
    }
}
