package tasks.lesson_20.task1;

import java.util.ArrayList;
import java.util.List;

class PythagorasChecker {
    private int max;
    private List<Triangle> triples = new ArrayList<>();

    PythagorasChecker(int max) {
        this.max = max;
    }

    List<Triangle> find() {
        for (int i = 1; i <= max; i++) {
            for (int j = i + 1; j <= max; j += i % 2 != 0 ? 2 : 1){
                if(gcd(i, j) == 1){
                    int a = j * j - i * i;
                    int b = 2 * i * j;
                    int c = i * i + j * j;
                    if(c > max) continue;
                    triples.add(new Triangle(a,b,c));
                }
            }
        }
        return triples;
    }


    int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }
}
