package tasks.lesson_20.task1;

class Triangle {
    private int c;
    private int b;
    private int a;

    public int getC() {
        return c;
    }

    public int getB() {
        return b;
    }

    public int getA() {
        return a;
    }

    Triangle(int c, int b, int a) {
        this.c = c;
        this.b = b;
        this.a = a;
    }

    @Override
    public String toString() {
        return String.format("a: %d\nb: %d\nc: %d\n", a,b,c);
    }
}
