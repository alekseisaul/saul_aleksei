package tasks.lesson_20.task6;

class PerfectNumberFinder {

    void find(int n){
        int s;
        for(int i = 2; i < n; i++){
            s = 0;
            for (int j = 1; j < i; j++) {
                if(i % j == 0){
                    s += j;
                }
            }
            if(s == i){
                System.out.println(i);
            }
        }
    }
}
