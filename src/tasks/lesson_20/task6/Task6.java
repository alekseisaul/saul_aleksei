package tasks.lesson_20.task6;

//Задача 6.(*) Совершенным числом называется число, равное сумме своих делителей, меньших его
//самого. Например, 28=1+2+4+7+14. Определите, является ли данное натуральное число
//совершенным. Найдите все совершенные числа на данном отрезке (возможно, стоит применить идею решета Эратосфена).

public class Task6 {
    public static void main(String[] args) {
        PerfectNumberFinder finder = new PerfectNumberFinder();
        finder.find(Integer.MAX_VALUE);
    }
}
