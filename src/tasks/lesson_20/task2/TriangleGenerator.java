package tasks.lesson_20.task2;

import java.util.ArrayList;
import java.util.List;

class TriangleGenerator {
    private List<Triangle> triangles = new ArrayList<>();
    private int maxS;

    TriangleGenerator(int maxS) {
        this.maxS = maxS;
    }

    List<Triangle> getTriangles(){
        generate();
        return triangles;
    }

    private void generate(){
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            for (int j = i + 1; j < Integer.MAX_VALUE; j += i % 2 != 0 ? 2 : 1){
//                if(gcd(i, j) == 1){
                    int a = j * j - i * i;
                    int b = 2 * i * j;
                    int c = i * i + j * j;
                    int s = a * b / 2;
                    if(s > maxS) return;
                    triangles.add(new Triangle(a,b,c));
//                }
            }
        }
    }

    private int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }
}
