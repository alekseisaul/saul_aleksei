package tasks.lesson_20.task2;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        TriangleGenerator generator = new TriangleGenerator(10000);
        List<Triangle> list = generator.getTriangles();
        for(Triangle tmp : list){
            System.out.println(tmp);
        }
    }
}
