package tasks.lesson_20.task2;

public class Triangle {
    private int c;
    private int b;
    private int a;
    private int s;

    Triangle(int a, int b, int c) {
        this.c = c;
        this.b = b;
        this.a = a;
        this.s = b * a / 2;
    }

    public int getC() {
        return c;
    }

    public int getB() {
        return b;
    }

    public int getA() {
        return a;
    }

    public int getS() {
        return s;
    }

    @Override
    public String toString() {
        return String.format("a: %d\nb: %d\nc: %d\nS: %d\n", a,b,c,s);
    }
}
