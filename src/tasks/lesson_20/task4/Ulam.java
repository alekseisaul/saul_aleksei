package tasks.lesson_20.task4;


class Ulam {
    private int size;
    private int[][] matrix;


    Ulam(int size) {
        this.size = size;
        matrix = new int[size][size];
    }

    int[][] getMatrix(){
        if(size % 2 == 0){
        fillUlam(size / 2 - 1, size / 2, true, 0, 1); //offset - смещение, увеличивается после 2 шагов
        } else {
            fillUlam(size / 2, size / 2, true, 0, 1); //iterator - значение, которре ставим в ячейку
        }

        return matrix;
    }

    private void fillUlam(int x, int y, boolean goR, int offset, int iterator){
        //если первая итерация - идём в начальную точку
        if(offset == 0){
            matrix[y][x] = iterator;
            //Go Right
            offset++;
            iterator++;
            fillUlam(x, y, true, offset, iterator);
            //Last Iteration
        } else if(offset == size){
            if(size % 2 != 0) {
                for (int i = 1; i < matrix.length; i++) {
                    matrix[y][i] = iterator;
                    iterator++;
                }
            } else {
                for(int i = 1; i < matrix.length; i++){
                    matrix[y][x - i] = iterator;
                    iterator++;
                }
            }
        } else {
            //Go Right
            if(goR) {
                for (int i = 0; i < offset; i++) {
                    x++;
                    matrix[y][x] = iterator;
                    iterator++;
                }
                //Go Up
                for (int i = 0; i < offset; i++) {
                    y--;
                    matrix[y][x] = iterator;
                    iterator++;
                }
                offset++;
                fillUlam(x, y, false, offset, iterator);
            }

//            Go Left
            else{
                for(int i = 0; i < offset; i++){
                    x--;
                    matrix[y][x] = iterator;
                    iterator++;
                }
                //Go Down
                for(int i = 0; i < offset; i++){
                    y++;
                    matrix[y][x] = iterator;
                    iterator++;
                }
                offset++;
                fillUlam(x, y, true, offset, iterator);
            }
        }
    }

    void printMatrix() {
        if(size % 2 == 0){
            fillUlam(size / 2 - 1, size / 2, true, 0, 1);
        } else {
            fillUlam(size / 2, size / 2, true, 0, 1);
        }

        for (int[] aM : matrix) {
            for (int anAM : aM) {
                if(testPrime(anAM)){
                    System.out.print(" *  ");
                } else {
                    System.out.print(String.format("%03d ", anAM));
                }
            }
            System.out.println();
        }

    }

    private static boolean testPrime(int number) {
        for(int i = 2; i < number; i++){
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }
}
