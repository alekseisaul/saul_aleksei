package tasks.lesson_20.task5;
//Задача 5. Б. Кордемский указывает одно интересное число 145,
// которое равно сумме факториалов своих цифр: 145=1!+4!+5!.
// Он пишет, что неизвестно, есть ли еще такие числа,
// удовлетворяющие названному условию.
// Написать программу по нахождению таких чисел.

public class Task5 {
    public static void main(String[] args) {
        NumberChecker checker = new NumberChecker();

        for(int i = 1; i <= Integer.MAX_VALUE; i++){
            if(checker.check(i)){
                System.out.println(i);
            }
        }
    }
}
