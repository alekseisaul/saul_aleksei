package tasks.lesson_20.task5;
//Задача 5. Б. Кордемский указывает одно интересное число 145,
// которое равно сумме факториалов своих цифр: 145=1!+4!+5!.
// Он пишет, что неизвестно, есть ли еще такие числа,
// удовлетворяющие названному условию.
// Написать программу по нахождению таких чисел.

class NumberChecker {

    boolean check(int n){
        int a;
        int sum = 0;
        int b = n;
        while(b != 0){
            a = b % 10;
            sum += factorial(a);
            b = b / 10;
        }
        return sum == n;
    }

    private int factorial(int n) {
        if(n == 0 || n == 1){
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }
}
