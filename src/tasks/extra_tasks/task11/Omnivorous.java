package tasks.extra_tasks.task11;

class Omnivorous extends Animal {
    Omnivorous() {
        super(Food.All);
    }

    @Override
    double getFoodConsumption() {
        return 75;
    }
}
