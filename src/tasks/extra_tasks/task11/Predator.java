package tasks.extra_tasks.task11;

class Predator extends Animal{
    Predator() {
        super(Food.Meat);
    }

    @Override
    double getFoodConsumption() {
        return 50;
    }
}
