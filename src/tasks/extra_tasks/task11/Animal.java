package tasks.extra_tasks.task11;

abstract class Animal {
    private Food foodType;

    Animal(Food foodType) {
        this.foodType = foodType;
    }

    abstract double getFoodConsumption();

    Food getFoodType() {
        return foodType;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
