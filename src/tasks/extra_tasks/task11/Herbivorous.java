package tasks.extra_tasks.task11;

class Herbivorous extends Animal {
    Herbivorous() {
        super(Food.Herbs);
    }

    @Override
    double getFoodConsumption() {
        return 100;
    }
}
