package tasks.extra_tasks.task9;
//Задача 9. Составить описание класса для представления времени.
// Предусмотреть возможности установки времени и изменения его отдельных полей (час, минута, секунда)
// с проверкой допустимости вводимых значений.
// В случае недопустимых значений полей выбрасываются исключения.
// Создать методы изменения времени на заданное количество часов, минут и секунд


public class Task9 {
    public static void main(String[] args) throws WrongTimeInput {
        Time time = new Time();
        time.printTime();
        time.setTime("01:45:59");
        time.setTime("15:45:00");
        time.printTime();
        time.addHour(50);
        time.printTime();
        time.addMinute(60);
        time.printTime();
        time.addSecond(3600);
        time.printTime();
        System.out.println(time.getTime());
    }
}
