package tasks.extra_tasks.task9;

class Time {
    private static final String WRONG_INPUT = "Wrong input!";
    private int time;

    void setTime(String str) throws WrongTimeInput {
        String[] input = str.split(":");

        int hour = Integer.parseInt(input[0]) * 3600;
        int minute = Integer.parseInt(input[1])* 60;
        int second = Integer.parseInt(input[2]);
        if(checkInput(Integer.parseInt(input[0]), 23)
                && checkInput(Integer.parseInt(input[1]), 59)
                && checkInput(Integer.parseInt(input[2]), 59)) {
            time = second + minute + hour;
        } else {
            throw new WrongTimeInput(WRONG_INPUT);
        }
    }

//    private void setHour(int hour) throws WrongTimeInput {
//        if(checkInput(hour, 23)) {
//            time += hour * 3600;
//        } else {
//            throw new WrongTimeInput(WRONG_FORMAT_HOUR);
//        }
//    }
//
//    private void setMinute(int minute) throws WrongTimeInput {
//        if(checkInput(minute, 59)) {
//            time += minute * 60;
//        } else {
//            throw new WrongTimeInput(WRONG_FORMAT_MINUTE);
//        }
//    }
//
//    private void setSecond(int second) throws WrongTimeInput {
//        if(checkInput(second, 59)) {
//            time += second;
//        } else {
//            throw new WrongTimeInput(WRONG_FORMAT_SECOND);
//        }
//    }

    void addHour(int hour) {
        if(hour > 0) {
            time += hour * 3600;
        }
    }

    void addMinute(int minute) {
        if(minute > 0){
            time += minute * 60;
        }
    }

    void addSecond(int second){
        if(second > 0){
            time += second;
        }
    }

    private boolean checkInput(int value, int max){
        return value >= 0 && value <= max;
    }

    int getTime() {
        return time;
    }

    private int getHour(){
        return (time / 3600);
    }

    private int getMinute(){
        return (((time - getHour() * 3600))/ 60);
    }

    private int getSecond(){
        return time - getHour() * 3600 - getMinute() * 60;
    }

    void printTime(){
        System.out.println(String.format("Current time is: %02d:%02d:%02d", getHour() % 24,getMinute(),getSecond()));
    }
}
