package tasks.extra_tasks.task9;

class WrongTimeInput extends Exception {
    WrongTimeInput(String message) {
        super(message);
    }
}
