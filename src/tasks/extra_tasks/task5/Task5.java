package tasks.extra_tasks.task5;

//Задача 5.Создать квадратную матрицу,
// на диагонали которой находятся тройки, выше диагонали находятся двойки,
// остальные элементы равны единице.

public class Task5 {
    public static void main(String[] args) throws Exception {
        MatrixCreator creator = new MatrixCreator();
        int[][] matrix = creator.create(5);
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix){
        for (int[] aMatrix : matrix) {
            for (int anAMatrix : aMatrix) {
                System.out.print(anAMatrix);
            }
            System.out.println();
        }
    }
}
