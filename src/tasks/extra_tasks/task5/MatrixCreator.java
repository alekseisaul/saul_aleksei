package tasks.extra_tasks.task5;

class MatrixCreator {

    int[][] create(int size) throws Exception {

        if(size <= 0){
            throw new Exception("Wrong Size!");
        }

        int[][] matrix = new int[size][size];

        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                  if(i == j){
                      matrix[i][j] = 3;
                  } else if((i - j) < 0){
                      matrix[i][j] = 2;
                  } else if((i - j) > 0){
                      matrix[i][j] = 1;
                  }
            }
        }
        return matrix;
    }
}
