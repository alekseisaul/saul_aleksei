package tasks.extra_tasks.task3;

//Задача 3. Дано предложение. Заменить группы пробелов одиночными, крайние пробелы удалить.
// Все слова перевести в нижний регистр, первые буквы сделать заглавными.

public class Task3 {
    public static void main(String[] args) {
        String str = "  aFas Sffb  FFF dEEE    e f";
        System.out.println(str);
        WhiteSpaceReplacer whiteSpaceReplacer = new WhiteSpaceReplacer();
        str = whiteSpaceReplacer.replaceWhateSpaces(str);
        System.out.println(str);
    }
}
