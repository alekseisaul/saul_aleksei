package tasks.extra_tasks.task3;

class WhiteSpaceReplacer {

    private static final String ERROR_THE_STRING_IS_EMPTY = "Error! The string is empty!";

    String replaceWhateSpaces(String str){
        if(str.isEmpty()){
            System.err.println(ERROR_THE_STRING_IS_EMPTY);
            return str;
        }

        StringBuilder sb = new StringBuilder();

        if(str.contains(" ") && str.length() > 1) {
            str = str.trim().replaceAll(" +", " ");
            String[] words = str.split(" ");
            for (String tmp : words) {
                if (tmp.length() > 1) {
                    sb.append(tmp.substring(0, 1).toUpperCase()).append(tmp.substring(1).toLowerCase()).append(" ");
                } else if(tmp.length() == 1){
                    sb.append(tmp.toUpperCase()).append(" ");
                }
            }
        }
        return sb.toString().trim();
    }
}
