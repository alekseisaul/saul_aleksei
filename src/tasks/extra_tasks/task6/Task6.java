package tasks.extra_tasks.task6;
//ThreadLocalRandom.current().nextInt(min, max);
//Задача 6. Сформировать квадратную матрицу n x n,
//на диагонали которой находятся случайные числа из диапазона [1; 9],
//а остальные числа равны 1.


public class Task6 {
    public static void main(String[] args) {
        MatrixCreator creator = new MatrixCreator();
        int n = 3;
        int[][] matrix = creator.create(n);
        printMaxtrix(matrix);

    }

    private static void printMaxtrix(int[][] matrix){
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix.length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
