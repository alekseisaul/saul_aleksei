package tasks.extra_tasks.task6;

import java.util.concurrent.ThreadLocalRandom;

class MatrixCreator {
    int[][] create(int n){
        int[][] matrix = new int[n][n];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if(i == j){
                    int min = 1;
                    int max = 9;
                    matrix[i][j] = ThreadLocalRandom.current().nextInt(min, max + 1);
                } else {
                    matrix[i][j] = 1;
                }
            }
        }
        return matrix;
    }
}
