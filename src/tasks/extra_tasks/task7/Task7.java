package tasks.extra_tasks.task7;

//Задача 7. Создать класс с двумя переменными.
// Добавить функцию вывода на экран и функцию изменения этих переменных.
// Добавить функцию, которая находит сумму значений этих переменных,
// и функцию которая находит наибольшее значение из этих двух переменных.


public class Task7 {
    public static void main(String[] args) {
        TwoNumbs numbs = new TwoNumbs(4l,10l);
        System.out.println(numbs.Sum());
        System.out.println(numbs.Max());
        numbs.print();
    }
}
