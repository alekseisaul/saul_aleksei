package tasks.extra_tasks.task7;

import javafx.util.Pair;

class TwoNumbs <T extends Number> {
    private T a;
    private T b;

    TwoNumbs(T a, T b) {
        this.a = a;
        this.b = b;
    }

    void setA(T a) {
        this.a = a;
    }

    void setB(T b) {
        this.b = b;
    }

    void print() {
        System.out.println(String.format("a: %d, b: %d", a, b));
    }

    <T extends Number> T Sum() {
        try {
            if (a instanceof Integer && b instanceof Integer) {
                return (T) new Integer(a.shortValue() + b.shortValue());
            } else if (a instanceof Long && b instanceof Long) {
                return (T) new Long(a.longValue() + b.longValue());
            } else if (a instanceof Float && b instanceof Float) {
                return (T) new Float(a.floatValue() + b.floatValue());
            } else if (a instanceof Double && b instanceof Double) {
                return (T) new Double(a.doubleValue() + b.doubleValue());
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        throw new ClassCastException("Wrong numbers type!");
    }

    <T extends Number> T Max() {
        try {
            if (a instanceof Integer && b instanceof Integer) {
                return (T) (Integer)Math.max(a.shortValue(), b.shortValue());
            } else if (a instanceof Long && b instanceof Long) {
                return (T) (Long)Math.max(a.longValue(), b.longValue());
            } else if (a instanceof Float && b instanceof Float) {
                return (T) (Float)Math.max(a.floatValue(), b.floatValue());
            } else if (a instanceof Double && b instanceof Double) {
                return (T) (Double)Math.max(a.doubleValue(), b.doubleValue());
            }
        } catch (ClassCastException e){
            e.printStackTrace();
        }
        throw new ClassCastException("Wrong numbers type!");
    }
}
