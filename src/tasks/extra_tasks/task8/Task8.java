package tasks.extra_tasks.task8;

//Задача 8. Описать класс, представляющий треугольник.
//Предусмотреть методы для создания объектов, вычисления площади, периметра и точки пересечения медиан.

public class Task8 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(new Node(0, 0), new Node(3, 2), new Node(7, 5));
        System.out.println(triangle.getArea());
        System.out.println(triangle.getPerimeter());
        System.out.println(triangle.getMedianIntersection());

        Triangle triangle2 = new Triangle(new Node(-1, 4), new Node(-1, 2), new Node(-7, 3));
        System.out.println(triangle2.getArea());
        System.out.println(triangle2.getPerimeter());
        System.out.println(triangle2.getMedianIntersection());
    }

}
