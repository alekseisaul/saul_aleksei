package tasks.extra_tasks.task8;

class Triangle {
    private Node node1;
    private Node node2;
    private Node node3;

    Triangle(Node node1, Node node2, Node node3) {
        this.node1 = node1;
        this.node2 = node2;
        this.node3 = node3;
    }

    double getArea(){
        return Math.abs(0.5 * (((node1.getX() - node3.getX()) * (node2.getY() - node3.getY())) -
                (((node2.getX() - node3.getX()) * (node1.getY() - node3.getY())))));
    }

    double getPerimeter(){
        double AB = Math.abs(Math.hypot((node1.getX() - node2.getX()), (node1.getY() - node2.getY())));
        double AC = Math.abs(Math.hypot((node1.getX() - node3.getX()), (node1.getY() - node3.getY())));
        double BC = Math.abs(Math.hypot((node2.getX() - node3.getX()), (node2.getY() - node3.getY())));
        return AB + AC + BC;
    }

    Node getMedianIntersection(){
        double x = (node1.getX() + node2.getX() + node3.getX()) / 3;
        double y = (node1.getY() + node2.getY() + node3.getY()) / 3;
        return new Node(x, y);
    }

}
