package tasks.extra_tasks.task10;

public class Abonent {

    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private int cardNum;
    private int debt;
    private int cred;
    private double intercityCallsDuration;
    private double cityCallsDuration;

    Abonent(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        id = ID.getID();
    }

    int getCardNum() {
        return cardNum;
    }

    void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    int getDebt() {
        return debt;
    }

    void setDebt(int debt) {
        this.debt = debt;
    }

    int getCred() {
        return cred;
    }

    void setCred(int cred) {
        this.cred = cred;
    }

    double getIntercityCallsDuration() {
        return intercityCallsDuration;
    }

    void setIntercityCallsDuration(double intercityCallsDuration) {
        this.intercityCallsDuration = intercityCallsDuration;
    }

    double getCityCallsDuration() {
        return cityCallsDuration;
    }

    void setCityCallsDuration(double cityCallsDuration) {
        this.cityCallsDuration = cityCallsDuration;
    }

    @Override
    public String toString() {
        return String.format("\nID: %08d\nFirst name: %s\nLast name: %s\nAddress: %s" +
                "\nCard number: %d\nDebt: %d\nCredit: %d" +
                "\nIntercity calls duration %g\nCity Calls %g"
                ,id,firstName,lastName,address,cardNum,debt,cred,intercityCallsDuration, cityCallsDuration);
    }
}
