package tasks.extra_tasks.task10;

class ID {
    private static ID INSTANCE = new ID();
    private static long idNum = 0L;

    private ID() {
    }

    static synchronized long getID(){
        if(INSTANCE == null){
            INSTANCE = new ID();
            return idNum;
        }
        idNum += 1L;
        return idNum;
    }
}
