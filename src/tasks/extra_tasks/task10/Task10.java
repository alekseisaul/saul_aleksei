package tasks.extra_tasks.task10;

//Задача 10. Класс Абонент:
// Идентификационный номер,
// Фамилия, Имя, Отчество,
// Адрес, Номер кредитной карточки,
// Дебет, Кредит, Время междугородных и городских переговоров;
// Конструктор; Методы: установка значений атрибутов,
// получение значений атрибутов, вывод информации.
// Создать массив объектов данного класса. Вывести сведения относительно абонентов,
// у которых время городских переговоров превышает заданное.
// Сведения относительно абонентов, которые пользовались междугородной связью.

public class Task10 {
    public static void main(String[] args) {
        Abonent[] abonents = new Abonent[2];
        abonents[0] = new Abonent("Al", "Sa", "Penza");
        abonents[1] = new Abonent("Ma", "Sa", "Penza");

        abonents[0].setIntercityCallsDuration(60);
        abonents[1].setIntercityCallsDuration(30);

        for(Abonent tmp : abonents){
            if(tmp.getIntercityCallsDuration() > 0) {
                System.out.println(tmp);
            }
        }
    }
}
