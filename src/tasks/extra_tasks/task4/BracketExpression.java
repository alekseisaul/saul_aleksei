package tasks.extra_tasks.task4;

class BracketExpression extends Exception {
    BracketExpression(String message) {
        super(message);
    }
}
