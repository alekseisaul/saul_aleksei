package tasks.extra_tasks.task4;

class ExpressionSolver {

    private static final String WRONG_EXPRESSION = "Wrong expression!";

    String solve(String expr) throws BracketExpression {
        BracketsChecker checker = new BracketsChecker(expr);
        if(checker.check()){
            expr = expr.toLowerCase();
            if(checker.getOpenBracketsCount() == 1){
                if(expr.contains("max")){
                    return Integer.toString(replaceMax(expr));
                } else if(expr.contains("min")){
                    return Integer.toString(replaceMin(expr));
                } else {
                    throw new BracketExpression(WRONG_EXPRESSION);
                }

            } else if(checker.getOpenBracketsCount() > 1) {
                //ищем левую границу для заменяемого выражения. -3 потому что учитываем слово min/max
                int leftIndex = expr.lastIndexOf("(") - 3;

                //ищем правую границу для заменяемого выражения
                int rightIndex = expr.substring(leftIndex).indexOf(")") + leftIndex + 1;

                //Заменяемое выражение
                String replaceTarget = expr.substring(leftIndex, rightIndex);
                String replacement = solve(replaceTarget);

                while(expr.contains(replaceTarget)){
                    expr = expr.replace(replaceTarget, replacement);
                }

                if(expr.contains("(") || expr.contains(")")){
                    expr = solve(expr);
                }
            }
        } else {
            throw new BracketExpression(WRONG_EXPRESSION);
        }
        return expr;
    }

    private int replaceMax(String expr){
        expr = expr.replace("max(", "").replace(")", "");
        String[] args = expr.split(",");
        return Math.max(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    private int replaceMin(String expr){
        expr = expr.replace("min(", "").replace(")", "");
        String[] args = expr.split(",");
        return Math.min(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }
}
