package tasks.extra_tasks.task4;

//Задача 4. Пользователь вводит формулу вида max(a,b) или min(a,b),
// где a,b - целые числа или аналогичные выражения min(), max().
// Найти значение выражения.
// Примеры: max(1,5) = 5, max(4,min(3,6)) = 4, min(max(10,max(5,3)),max(9,0)) = 9, min(max(1,max(5,max(1,3))),min(9,0)) = 0


public class Task4 {
    public static void main(String[] args) throws BracketExpression {
//        String expression = "max(1,5)";
//        String expression = "max(4,min(3,6))";
//        String expression = "min(max(10,max(5,3)),max(9,0))";
        String expression = "min(min(1,2),min(1,min(1,2)))";
        ExpressionSolver expressionSolver = new ExpressionSolver();
        System.out.println(expressionSolver.solve(expression));
    }
}
