package tasks.extra_tasks.task4;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;

class BracketsChecker {
    private static final  Map<Character, Integer> OPEN_BRACKETS_MAP = new HashMap<>();
    private static final  Map<Character, Integer> CLOSE_BRACKET_MAP = new HashMap<>();
    private static final Map<Character, Pair<Character, Character>> PAIRS = new HashMap<Character, Pair<Character, Character>>();
    private String str;

    int getOpenBracketsCount() {
        return openBracketsCount;
    }

    public int getCloseBracketsCount() {
        return closeBracketsCount;
    }

    private int openBracketsCount;
    private int closeBracketsCount;

    static{
        char[] openBrackets = {'[', '(', '{', '<'};
        char[] closeBrackets = {']', ')', '}', '>'};
        for(int i = 0; i < openBrackets.length; i++){
            OPEN_BRACKETS_MAP.put(openBrackets[i], 0);
            CLOSE_BRACKET_MAP.put(closeBrackets[i], 0);
            PAIRS.put(closeBrackets[i],new Pair<>(closeBrackets[i], openBrackets[i]));
        }

        for(char tmp : openBrackets){
            OPEN_BRACKETS_MAP.put(tmp, 0);
        }

        for(char tmp : closeBrackets){
            CLOSE_BRACKET_MAP.put(tmp, 0);
        }
    }

    BracketsChecker(String str) {
        this.str = str;
    }

    boolean check(){
        if(CLOSE_BRACKET_MAP.keySet().contains(str.charAt(0))){
            return false;
        }

        char[] chars = str.toCharArray();

        for(char tmp : chars){
            if(OPEN_BRACKETS_MAP.keySet().contains(tmp)){
                OPEN_BRACKETS_MAP.put(tmp, (OPEN_BRACKETS_MAP.get(tmp)+1));
                openBracketsCount++;
            } else if(CLOSE_BRACKET_MAP.keySet().contains(tmp)){
                CLOSE_BRACKET_MAP.put(tmp, (CLOSE_BRACKET_MAP.get(tmp)+1));
                if(CLOSE_BRACKET_MAP.get(tmp) > OPEN_BRACKETS_MAP.get(
                        PAIRS.get(tmp).getValue()
                )){
                    return false;
                }
                closeBracketsCount++;
            }
        }

        return openBracketsCount == closeBracketsCount;
    }
}
