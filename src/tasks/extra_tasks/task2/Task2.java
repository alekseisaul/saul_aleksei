package tasks.extra_tasks.task2;

//Задача 2. Дан массив. Перемешать его элементы случайным образом так, чтобы каждый элемент оказался на новом месте.

import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

public class Task2 {
    public static void main(String[] args) {
        int[] array = new int[10];
        fillArray(array);
        Mesher mesher1 = new Mesher(array);
        int[] array2 = array.clone();
        Mesher mesher2 = new Mesher(array2);

        System.out.println("\nДо:");
        printArray(array);
        mesher1.mesh();
        mesher2.mesh2();
        System.out.println("\nПосле mesh1:");
        printArray(array);
        System.out.println("\nПосле mesh2:");
        printArray(array2);

    }

    private static void fillArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, 100);
        }
    }

    private static void printArray(int[] array) {
        for (int tmp : array) {
            System.out.print(tmp + " ");
        }
        System.out.println();
    }
}
