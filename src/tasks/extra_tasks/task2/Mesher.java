package tasks.extra_tasks.task2;

import java.util.concurrent.ThreadLocalRandom;

class Mesher {
    private int[] array;

    Mesher(int[] array) {
        this.array = array;
    }

    void mesh() {
        boolean[] indexStatus = new boolean[array.length];
        int newIndex;
        for(int i = 0; i < array.length; i++) {
            do {
                newIndex = ThreadLocalRandom.current().nextInt(0, array.length);
            } while (indexStatus[newIndex]);
            indexStatus[newIndex] = true;
            swap(array, newIndex, i);
        }
    }

    void mesh2(){
        boolean[] indexStatus = new boolean[array.length];
        int oldIndex;
        int newIndex;
        while (!checkStatus(indexStatus)) {
            do {
                oldIndex = ThreadLocalRandom.current().nextInt(0, array.length);
            } while (indexStatus[oldIndex]);
            do {
                newIndex = ThreadLocalRandom.current().nextInt(0, array.length);
            } while (indexStatus[newIndex]);
            swap(array, newIndex, oldIndex);
            indexStatus[oldIndex] = true;
            indexStatus[newIndex] = true;
        }
    }

    private void swap(int[] array, int newIndex, int i) {
        int tmp = array[newIndex];
        array[newIndex] = array[i];
        array[i] = tmp;
    }

    private boolean checkStatus(boolean[] indexStatus){
        boolean status = false;
        if(indexStatus.length > 1){
            for(int i = 1; i < indexStatus.length; i++){
                status = indexStatus[i-1] & indexStatus[i];
            }
        } else if(indexStatus.length == 1){
            status = indexStatus[0];
        }
        return status;
    }
}
