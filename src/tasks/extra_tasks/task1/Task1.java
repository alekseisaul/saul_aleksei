package tasks.extra_tasks.task1;

//1. Компьютер загадывает число от 1 до n.
// У пользователя k попыток отгадать.
// После каждой неудачной попытки компьютер сообщает меньше или больше загаданное число.
// В конце игры текст с результатом (или “Вы угадали”, или “Попытки закончились”).

public class Task1 {
    public static void main(String[] args) {
        for(int i = 0; i < 1; i++) {
            NumFinder numFinder = new NumFinder(1, Integer.MAX_VALUE, 10);
            numFinder.find();
        }
    }
}
