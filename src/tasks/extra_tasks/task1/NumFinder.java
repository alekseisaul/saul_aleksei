package tasks.extra_tasks.task1;

import java.util.concurrent.ThreadLocalRandom;

class NumFinder {
    private static final String FAIL = "Попытки закончились. Использовано %d попыток, Загаданное число: %d";
    private static final String WIN = "Вы угадали. Число %d. Количество ходов %d";
    private static final String BIGGER = "Больше";
    private static final String LESS = "Меньше";
    private int count;
    private int maxCount;
    private int randomNum;
    private int max;
    private int min;

    NumFinder(int min, int max, int maxCount) {
        this.min = min;
        this.max = max;
        this.maxCount = maxCount;
        this.randomNum = ThreadLocalRandom.current().nextInt(min, max);
    }

    void find(){
        int mid = (min + max) / 2;
        if(count >= maxCount){
            System.out.println(String.format(FAIL, count, randomNum));
        } else {
            if (mid == randomNum) {
                count++;
                System.out.println(String.format(WIN, mid, count));
            } else if (mid > randomNum) {
                System.out.println(BIGGER);
                count++;
                max = mid - 1;
                find();
            } else {
                System.out.println(LESS);
                count++;
                min = mid + 1;
                find();
            }
        }
    }
}
