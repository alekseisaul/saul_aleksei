package tasks.extra_tasks.task1.linked_list;

public class Main {
    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        for(int i = 0; i < 10; i++) {
            linkedList.add(new MyLinkedList.MyNode(Integer.toString(i)));
        }
        MyLinkedList.MyNode head = linkedList.head();
        MyLinkedList.MyNode current = head;
        int length = 0;
        MyLinkedList.MyNode middle = head;

        while(current.next() != null){
            length++;
            if(length%2 ==0){
                middle = middle.next();
            }
            current = current.next();
        }

        if(length%2 == 1){
            middle = middle.next();
        }
        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : " + middle);
    }
}
