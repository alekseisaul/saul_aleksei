package tasks.extra_tasks.task1.linked_list;

public class MyLinkedList {
    private MyNode head;
    private MyNode tail;

    MyLinkedList() {
        this.head = new MyNode("head");
        this.tail = head;
    }

    MyNode head(){
        return head;
    }
    public void add(MyNode node){
        tail.setNext(node);
        tail = node;
    }

    public static class MyNode {
        private MyNode next;
        private MyNode prev;
        private String data;

        MyNode(String data) {
            this.data = data;
        }

        public String data() {
            return data;
        }
        public void setData(String data) {
            this.data = data;
        }
        MyNode next() {
            return next;
        }

        public MyNode prev() {
            return prev;
        }
        void setNext(MyNode next) {
            this.next = next;
        }
        public void setPrev(MyNode prev) {
            this.prev = prev;
        }

        public String toString(){
            return this.data;
        }
    }
}
