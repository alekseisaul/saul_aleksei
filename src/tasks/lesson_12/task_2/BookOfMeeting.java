package tasks.lesson_12.task_2;

//2) На основе первой задачи. Создать класс BookOfMeeting.
// В нём создать массив из 365 дней.
// Заполнить каждый день значением дня недели и месяца и задать часы и минуты встречи равные -1.
// Это означает, что в этот день встречи нет.
//-Добавить в класс функцию добавить встречу в календарь.
// Она должна проверять введенные даты и если в этот день нет встреч, то устанавливать заданное время начала встречи.
//-Добавить в класс функцию показать все имеющиеся встречи,
// которая выводит на экран все дни со временем встреч. Дни в которые встреч нет печатать не нужно.

//import tasks.lesson_12.task_1.DAY;
//import tasks.lesson_12.task_1.DateOfMeet;
//import tasks.lesson_12.task_1.MONTH;
//import tasks.lesson_12.task_1.Util;
//import static tasks.lesson_12.task_1.Util.checkInput;


public class BookOfMeeting {
    private static final DateOfMeet[] CALENDAR = new DateOfMeet[365];
    private static final String INPUT_A_MONTH_NUMBER = "Input a month number. For example: 1 for January:";
    private static final String INPUT_A_DAY_NUMBER = "Input a day number";
    private static final String DAY_IS_BUSY = "This day is Busy!";
    private static final String INPUT_A_MEETING_NAME = "Input a meeting name";
    private static final String INPUT_AN_HOUR = "Input an hour, from 0 to 23";
    private static final String INPUT_A_MINUTE = "Input a minute, from 0 to 59";
    private static final String INPUT_A_DESCRIPTION = "Input a description for the meeting";

    BookOfMeeting() {
        int yearDay = 0;
        for (int i = 0; i < MONTH.values().length; i++) {
            for (int j = 0; j < MONTH.getMonth(i).getDaysCount(); j++) {
                CALENDAR[yearDay] = new DateOfMeet(MONTH.getMonth(i), DAY.getDay(j % 7), j+1, yearDay);
                yearDay++;
            }
        }
        System.out.println();
    }

    void addMeeting() {
//            System.out.println("Input a meeting name");
//            String meetingName = Util.readFromConsole();


        System.out.println(INPUT_A_MONTH_NUMBER);
        int monthNumber = Integer.parseInt(Util.readFromConsole());
        if (Util.checkInput(monthNumber, 1, 12)) {
            return;
        }
        MONTH month = MONTH.getMonth(monthNumber - 1);

        System.out.println(INPUT_A_DAY_NUMBER);
        int monthDay = Integer.parseInt(Util.readFromConsole());
        if (Util.checkInput(monthDay, 1, 31)) {
            return;
        }

        if (isDayBusy(countYearDay(monthNumber, monthDay))) {
            System.err.println(DAY_IS_BUSY);
            return;
        }

        System.out.println(INPUT_A_MEETING_NAME);
        String meetingName = Util.readFromConsole();


//            System.out.println("Input a week day. For example: 1 for monday");
//            int dayNumber = Integer.parseInt(Util.readFromConsole());
//            if (Util.checkInput(dayNumber, 1, 7)) {
//                return;
//            }
//            DAY day = DAY.getDay(dayNumber - 1);


        System.out.println(INPUT_AN_HOUR);
        int hour = Integer.parseInt(Util.readFromConsole());
        if (Util.checkInput(hour, 0, 23)) {
            return;
        }

        System.out.println(INPUT_A_MINUTE);
        int minute = Integer.parseInt(Util.readFromConsole());
        if (Util.checkInput(minute, 0, 59)) {
            return;
        }

        System.out.println(INPUT_A_DESCRIPTION);
        String desc = Util.readFromConsole();

        CALENDAR[countYearDay(monthNumber, monthDay)].setHour(hour);
        CALENDAR[countYearDay(monthNumber, monthDay)].setMinute(minute);
        CALENDAR[countYearDay(monthNumber, monthDay)].setMeetingName(meetingName);
        CALENDAR[countYearDay(monthNumber, monthDay)].setDescription(desc);
    }

    void printAllMeetings(){
        for(DateOfMeet tmp : CALENDAR){
            if(tmp.getHour() != -1){
                System.out.println(tmp);
                System.out.println();
            }
        }
    }

    private boolean isDayBusy(int day){
        return CALENDAR[day].getHour() != -1 || CALENDAR[day].getMinute() != -1;
    }

    private int countYearDay(int month, int monthDay){
        int yearDay = 0;
        for(int i = 0; i < month; i++){
            for(int j = 0; j < monthDay; j++)
                yearDay++;
        }
        return yearDay;
    }

    static DateOfMeet[] getCALENDAR() {
        return CALENDAR;
    }
}
