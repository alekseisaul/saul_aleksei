package tasks.lesson_12.task_2;


public class DateOfMeet {
    private MONTH month;
    private DAY weekDay;
    private String meetingName;
    private String description;
    private int monthDay;

    void setHour(int hour) {
        this.hour = hour;
    }

    void setMinute(int minute) {
        this.minute = minute;
    }

    private int hour;
    private int minute;

//    DateOfMeet(String meetingName, MONTH month, DAY weekDay,
//               int hour, int minute, String description) {
//        this.month = month;
//        this.weekDay = weekDay;
//        this.meetingName = meetingName;
//        this.description = description;
//        this.hour = hour;
//        this.minute = minute;
//    }

    void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    void setDescription(String description) {
        this.description = description;
    }

    int getHour() {
        return hour;
    }

    int getMinute() {
        return minute;
    }

    DateOfMeet(MONTH month, DAY weekDay, int monthDay, int yearDay) {
        this.month = month;
        this.weekDay = weekDay;
        this.monthDay = monthDay;
        hour = -1;
        minute = -1;
    }

//    void show(){
//        System.out.println(this.toString());
//    }

    @Override
    public String toString() {
        return String.format("Meeting %s:\nDate: %s, %s\nDay: %s;\nTime: %02d:%02d;\nDescription: %s;",
                meetingName, month,monthDay, weekDay, hour,minute, description);
    }
}
