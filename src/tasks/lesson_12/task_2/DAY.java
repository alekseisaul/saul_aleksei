package tasks.lesson_12.task_2;

public enum DAY {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    private int number;

    DAY(int number) {
        this.number = number;
    }

    public static DAY getDay(int i){
        return DAY.values()[i];
    }

    public static DAY getDay(String s){
        return DAY.valueOf(s);
    }

    public static DAY getNextDay(){
        return null;
    }
}
