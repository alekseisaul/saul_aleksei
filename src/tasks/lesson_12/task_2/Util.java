package tasks.lesson_12.task_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Util {

//    private static final String WRONG_INPUT = "Wrong input!!!";
    private static final String WRONG_INPUT_FORMAT = "Wrong input format!";

    static String readFromConsole() {
        String input = "";

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            System.err.println(WRONG_INPUT_FORMAT);
        }

        return input;
    }

    static boolean checkInput(int value, int min, int max) {
        if(value < min || value > max){
            System.err.println(WRONG_INPUT_FORMAT);
            return true;
        }
        return false;
    }
}

