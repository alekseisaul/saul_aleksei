package tasks.lesson_12.task_1;

 enum MONTH {
    JANUARY(31),
    FEBRUARY(28),
    MARCH(31),
    APRIL(30),
    MAY(31),
    JUNE(30),
    JULY(31),
    AUGUST(31),
    SEPTEMBER(30),
    OCTOBER(31),
    NOVEMBER(30),
    DECEMBER(31);

    private int daysCount;

    MONTH(int daysCount) {
        this.daysCount = daysCount;
    }

     int getDaysCount() {
        return daysCount;
    }

     static MONTH getMonth(int i){
        return MONTH.values()[i];
    }

     static MONTH getMonth(String s){
        return MONTH.valueOf(s.toUpperCase());
    }

}
