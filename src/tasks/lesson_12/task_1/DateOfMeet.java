package tasks.lesson_12.task_1;

 class DateOfMeet {
    private MONTH month;
    private DAY weekDay;
    private String meetingName;
    private String description;
    private int monthDay;

     void setHour(int hour) {
        this.hour = hour;
    }

     void setMinute(int minute) {
        this.minute = minute;
    }

    private int hour;
    private int minute;

    DateOfMeet(String meetingName, MONTH month, DAY weekDay,
               int hour, int minute, String description) {
        this.month = month;
        this.weekDay = weekDay;
        this.meetingName = meetingName;
        this.description = description;
        this.hour = hour;
        this.minute = minute;
    }

     DateOfMeet(MONTH month, DAY weekDay, int monthDay) {
        this.month = month;
        this.weekDay = weekDay;
        this.monthDay = monthDay;
        hour = -1;
        minute = -1;
    }

    void show(){
//        System.out.println(String.format("Meeting %s:\nDate: %s/%s\nTime: %02d:%02d;\nDescription: %s;",
//                meetingName, month, weekDay, hour,minute, description));
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return String.format("Meeting %s:\nMonth: %s;\nDay of week: %s;\nTime: %02d:%02d;\nDescription: %s;",
                meetingName, month, weekDay, hour,minute, description);
    }
}
