package tasks.lesson_12.task_1;

//1)Создать два enum - класса. В одном перечислены все дни недели, в другом все месяца.
//Создать класс DateOfMeet в котором будут поля этих enum - классов, поле с названием встречи,
//поле с описание встречи и два поля с часами и минутами.
//Добавить в него метод show который показывает информацию о встрече.
//Создать массив из пяти таких встреч и заполнить их с клавиатуры в цикле.

//2) На основе первой задачи. Создать класс BookOfMeeting.
// В нём создать массив из 365 дней.
// Заполнить каждый день значением дня недели и месяца и задать часы и минуты встречи равные -1.
// Это означает, что в этот день встречи нет.
//-Добавить в класс функцию добавить встречу в календарь.
// Она должна проверять введенные даты и если в этот день нет встреч, то устанавливать заданное время начала встречи.
//-Добавить в класс функцию показать все имеющиеся встречи,
// которая выводит на экран все дни со временем встреч. Дни в которые встреч нет печатать не нужно.


import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String WRONG_INPUT_FORMAT = "Wrong input format!";
    private static final String INPUT_A_MEETING_NAME = "Input a meeting name";
    private static final String INPUT_A_MONTH_NUMBER = "Input a month number. For example: 1 for January:";
    private static final String INPUT_A_WEEK_DAY = "Input a week day. For example: 1 for monday";
    private static final String INPUT_AN_HOUR = "Input an hour, from 0 to 23";
    private static final String INPUT_A_MINUTE = "Input a minute, from 0 to 59";
    private static final String INPUT_A_DESCRIPTION = "Input a description for the meeting";

    public static void main(String[] args) {
        for(int i = 0; i < 31; i++) {
            System.out.println(i % 7);
        }
        List<DateOfMeet> meetings = new ArrayList<>();

        for(int i = 0; i < 1; i++) {
            System.out.println(INPUT_A_MEETING_NAME);
            String meetingName = Util.readFromConsole();


            System.out.println(INPUT_A_MONTH_NUMBER);
            int monthNumber = Integer.parseInt(Util.readFromConsole());
            if(checkInput(monthNumber, 1, 12)){
                return;
            }
            MONTH month = MONTH.getMonth(monthNumber - 1);

            System.out.println(INPUT_A_WEEK_DAY);
            int dayNumber = Integer.parseInt(Util.readFromConsole());
            if(checkInput(dayNumber, 1, 7)){
                return;
            }
            DAY day = DAY.getDay(dayNumber - 1);


            System.out.println(INPUT_AN_HOUR);
            int hour = Integer.parseInt(Util.readFromConsole());
            if(checkInput(hour, 0, 23)){
                return;
            }

            System.out.println(INPUT_A_MINUTE);
            int minute = Integer.parseInt(Util.readFromConsole());
            if(checkInput(minute, 0, 59)){
                return;
            }

            System.out.println(INPUT_A_DESCRIPTION);
            String desc = Util.readFromConsole();
            meetings.add(new DateOfMeet(meetingName, month, day, hour, minute, desc));
        }

        for(DateOfMeet meeting : meetings){
            meeting.show();
        }
//        BookOfMeeting meeting = new BookOfMeeting();
    }

    private static boolean checkInput(int value, int min, int max) {
        if(value < min || value > max){
            System.err.println(WRONG_INPUT_FORMAT);
            return true;
        }
        return false;
    }
}
