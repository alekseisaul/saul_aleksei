package tasks.lesson_12.task_1;

 enum DAY {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    private int number;

    DAY(int number) {
        this.number = number;
    }

     static DAY getDay(int i){
        return DAY.values()[i];
    }

     static DAY getDay(String s){
        return DAY.valueOf(s);
    }

     static DAY getNextDay(){
        return null;
    }
}
