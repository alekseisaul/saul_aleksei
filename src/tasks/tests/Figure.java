package tasks.tests;

class Figure {
    private static final String LINE = "Line";
    private static final String SQUARE = "Square";
    private static final String PARALLELEPIPED = "Parallelepiped";
    private int x;
    private int y;
    private int z;

    Figure(int x) {
        this.x = x;
    }

    Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Figure(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    void showFigureType(){
        if(y == 0 && z == 0){
            System.out.println(LINE);
        } else if(z == 0){
            System.out.println(SQUARE);
        } else System.out.println(PARALLELEPIPED);
    }

    class MyException extends Exception{
        public MyException() {
        }


    }
}
