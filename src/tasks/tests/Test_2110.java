package tasks.tests;

public class Test_2110 {
    public static void main(String[] args) {
        //check if a figure is a line
        Figure figure = new Figure(10);
        figure.showFigureType();

        //check if a figure is a square
        figure = new Figure(10, 5);
        figure.showFigureType();

        //check if gigure is a parallelepiped
        figure = new Figure(10,5,1);
        figure.showFigureType();
    }
}
