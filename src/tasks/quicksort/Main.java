package tasks.quicksort;

import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        Quicksort quicksort = new Quicksort();

        int[] array = new int[50];
        for (int i = 0; i <array.length ; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, 100);
        }

        System.out.println("Было:");
        for (int i : array){
            System.out.print(i + " ");
        }
        System.out.println();

        try {
            quicksort.sort(array, 0, array.length-1);
        } catch (QuickSortException e) {
            e.printStackTrace();
        }
        System.out.println("Стало:");
        for (int i : array){
            System.out.print(i + " ");
        }
    }
}
