package tasks.quicksort;

class QuickSortException extends Exception {
    QuickSortException(String message) {
        super(message);
    }
}
