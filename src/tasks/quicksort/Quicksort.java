package tasks.quicksort;

class Quicksort {
    void sort(int[] array, int low, int high) throws QuickSortException{
        if(array.length == 0){
            throw new QuickSortException("Array length is 0!");
        }

        if(low >= high){
            throw new QuickSortException("There is nothing to do!");
        }

        int middle = low + (high - low) / 2;
        int base = array[middle];

        int i = low;
        int j = high;

        while(i <= j) {

            while (array[i] < base) {
                i++;
            }

            while (array[j] > base) {
                j--;
            }

            if (i <= j) {
                int tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
                i++;
                j--;
            }
        }

        if(low < j){
            sort(array, low, j);
        }

        if(high > i){
            sort(array, i, high);
        }
    }
}
