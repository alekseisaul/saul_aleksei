package tasks.lesson_15.task_10;

// Дана последовательность натуральных чисел (одно число в строке), завершающаяся числом 0.
// Определите наибольшее значение числа в этой последовательности.
// В этой задаче нельзя использовать глобальные переменные и передавать какие-либо параметры в рекурсивную функцию.
// Функция получает данные, считывая их с клавиатуры.
// Функция возвращает единственное значение: максимум считанной последовательности.
// Гарантируется, что последовательность содержит хотя бы одно число (кроме нуля).

import java.util.ArrayList;
import java.util.List;

public class Task_10 {
    private static final List<Integer> LIST = new ArrayList<>();

    private static void sort(){
        if(LIST.size() > 1){
            LIST.remove(Integer.valueOf(Math.min(LIST.get(0), LIST.get(LIST.size()-1))));
            sort();
        }
    }

    public static void main(String[] args) {
        int a;
        while((a = Integer.parseInt(Util.readFromConsole())) != 0){
            LIST.add(a);
        }
        sort();
        System.out.println(LIST.get(LIST.size()-1));
    }

}
