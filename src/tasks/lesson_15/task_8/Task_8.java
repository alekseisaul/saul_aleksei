package tasks.lesson_15.task_8;

public class Task_8 {
    public static void main(String[] args) {
        String s = "ANNNA";
        System.out.println(isPolyndrom(s));
    }

    private static boolean isPolyndrom(String s){
        char first = s.charAt(0);
        char last = s.charAt(s.length()-1);
        if(first == last) {
            if(s.length() > 2) {
                s = s.substring(1, s.length() - 1);
                return isPolyndrom(s);
            } else if (s.length() == 2){
                first = s.charAt(0);
                last = s.charAt(1);
                return first == last;
            } else {
                return true;
            }
        }
        return false;
    }
}