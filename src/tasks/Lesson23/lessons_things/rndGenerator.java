package tasks.Lesson23.lessons_things;

import java.util.concurrent.ThreadLocalRandom;

public class rndGenerator {
    public static int getRnd(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max);
    }
}
