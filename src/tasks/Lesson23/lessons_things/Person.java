package tasks.Lesson23.lessons_things;

import org.jetbrains.annotations.NotNull;

public abstract class Person {
    private int health;
    private String name;
    private int damage;
    private int defence;

    public Person(String name, int health)  {
        this.health = health;
        this.name = name;
        damage = 0;
        defence = 0;
    }

    public int getDamage() {
        return damage;
    }

    protected void setDamage(int damage) {
        this.damage = damage;
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    protected void setDefence(int defence) {
        this.defence = defence;
    }

    public int getDefence() {
        return defence;
    }

    @Override
    public String toString() {
        return String.format("\nName: %s\nHealth: %d\nDamage: %d\nDefence: %d\n", name, health, damage, defence);
    }

}
