package tasks.Lesson23.task2;
//2. Создать класс маг с полями количество здоровье, количество магии и имя.
// Написать компаратор для сравнения этих двух магов по количеству наносимого урона.
// Составить список самых сильных магов с использованием treeSet.


import tasks.Lesson23.lessons_things.Person;
import tasks.Lesson23.lessons_things.rndGenerator;

import java.util.Comparator;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

public class Task2 {
    public static void main(String[] args) {

        Mage mage1 = new Mage("Mage1", rndGenerator.getRnd(10, 100), rndGenerator.getRnd(10, 100));
        Mage mage2 = new Mage("Mage2", rndGenerator.getRnd(10, 100), rndGenerator.getRnd(10, 100));

        Comparator<Person> damageComp = Comparator.comparing(Person::getDamage);
        Comparator<Person> healthComp = Comparator.comparing(Person::getHealth);
        Comparator<Person> defenceComp = Comparator.comparing(Person::getDefence);

        Comparator<Person> damageHealthComp = damageComp.thenComparing(healthComp);
        Comparator<Person> damageHealthDefenceComp = damageHealthComp.thenComparing(defenceComp);

        TreeSet<Person> mages = new TreeSet<>(damageHealthDefenceComp);


        for(int i = 0; i < 10; i ++){
            mages.add(new Mage("Mage"+i, rndGenerator.getRnd(10, 100), rndGenerator.getRnd(10, 100)));
        }

        System.out.println(String.format("The weakness mage is: %s", mages.first().toString()));
        System.out.println(String.format("The strongest mage is: %s", mages.last().toString()));
    }
}
