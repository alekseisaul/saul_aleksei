package tasks.Lesson23.task2;

import tasks.Lesson23.lessons_things.Person;

//Создать класс маг с полями количество здоровье, количество магии и имя.

class Mage extends Person{
    private int mana;

    Mage(String name, int health, int mana) {
        super(name, health);
        this.mana = mana;
        int mageAttack = mana / 10;
        super.setDamage(mageAttack);
    }
}
