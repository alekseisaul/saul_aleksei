package tasks.Lesson23.task3;

//3. Создать класс рыцарь с полями количество здоровья, количество брони, количеством урона и именем.
// Провести рыцарский турнир и узнать кто победил используя treeSet,
// по критериям количество здоровья, количество брони и наносимый урон.

import tasks.Lesson23.lessons_things.*;

import java.util.*;

public class Task3 {
    public static void main(String[] args) {
        Comparator<Person> damageComp = Comparator.comparing(Person::getDamage);
        Comparator<Person> healthComp = Comparator.comparing(Person::getHealth);
        Comparator<Person> defenceComp = Comparator.comparing(Person::getDefence);

        Comparator<Person> damageHealthComp = damageComp.thenComparing(healthComp);
        Comparator<Person> damageHealthDefenceComp = damageHealthComp.thenComparing(defenceComp);

        TreeSet<Person> people = new TreeSet<>(damageHealthDefenceComp);


        List<Knight> knights = new ArrayList<>();

        for(int i = 0; i < 10; i++){
            people.add(new Knight("Knight"+i, rndGenerator.getRnd(10, 100),
                    rndGenerator.getRnd(10, 100), rndGenerator.getRnd(10, 100)));
        }

        System.out.println(String.format("The weakness warrior is: %s", people.first()));
        System.out.println(String.format("The strongest warrior is: %s", people.last()));
    }
}
