package tasks.Lesson23.task3;

import tasks.Lesson23.task1.Warrior;

//3. Создать класс рыцарь с полями количество здоровья, количество брони, количеством урона и именем.
// Провести рыцарский турнир и узнать кто победил используя treeSet,
// по критериям количество здоровья, количество брони и наносимый урон.

public class Knight extends Warrior {
    private int armor;

    Knight(String name, int health, int physicalAttack, int armor) {
        super(name, health, physicalAttack);
        this.armor = armor;
        super.setDefence(armor);
    }

    public int getArmor() {
        return armor;
    }
}
