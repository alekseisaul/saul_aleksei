package tasks.Lesson23.task1;

//1. Создать класс воин с полями количество здоровье, количество урона и имя.
// Написать компаратор для сравнения этих двух воинов по количеству наносимого урона.
// Найти самого слабого и самого сильного рыцаря из списка 10 рыцарей используя treeSet.

import tasks.Lesson23.lessons_things.Person;
import tasks.Lesson23.lessons_things.rndGenerator;

import java.util.*;

public class Task1 {
    public static void main(String[] args) {
        Warrior w1 = new Warrior("Warrior1", 100, 20);
        Warrior w2 = new Warrior("Warrior2",100, 20);

        Comparator<Person> damageComp = Comparator.comparing(Person::getDamage);
        Comparator<Person> healthComp = Comparator.comparing(Person::getHealth);
        Comparator<Person> defenceComp = Comparator.comparing(Person::getDefence);

        Comparator<Person> damageHealthComp = damageComp.thenComparing(healthComp);
        Comparator<Person> damageHealthDefenceComp = damageHealthComp.thenComparing(defenceComp);

        System.out.println(damageHealthComp.compare(w1, w2));

        TreeSet<Warrior> warriors = new TreeSet<>(damageComp);

        for(int i = 0; i < 10; i++){
            warriors.add(new Warrior("Warrior"+i, rndGenerator.getRnd(10, 100), rndGenerator.getRnd(10, 100)));
        }

        System.out.println(String.format("The weakness warrior is: %s", warriors.first()));
        System.out.println(String.format("The strongest warrior is: %s", warriors.last()));
    }
}
