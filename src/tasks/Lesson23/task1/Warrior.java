package tasks.Lesson23.task1;

//полями количество здоровье, количество урона и имя

import org.jetbrains.annotations.NotNull;
import tasks.Lesson23.lessons_things.Person;

public class Warrior extends Person {
    private int physicalAttack;

    protected Warrior(String name, int health, int physicalAttack) {
        super(name, health);
        this.physicalAttack = physicalAttack;
        super.setDamage(physicalAttack);
    }
}
