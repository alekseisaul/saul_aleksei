package tasks.brackets;

public class BracketsMain {
    public static void main(String[] args) {
        String str = "[[]<()<>>{}]";
        BracketsChecker checker = new BracketsChecker(str);
        System.out.println(checker.check() ? "OK" : "NOK");
    }
}