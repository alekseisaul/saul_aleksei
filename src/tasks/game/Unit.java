package tasks.game;

public abstract class Unit {
    private int hp;
    private int armor;
    private int damage;
    int x;
    int y;
    int numOfDraw;
    //inventory


    int getArmor() {
        return armor;
    }

    int getDamage() {
        return damage;
    }

    int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Unit(int hp, int armor, int damage) {
        this.hp = hp;
        this.armor = armor;
        this.damage = damage;
    }

    boolean canGo(Direction direction, int sizeMap) {
        int newX = x + direction.x;
        int newY = y + direction.y;
        return newX < sizeMap && newX >= 0 && newY < sizeMap && newY >= 0;
    }

    abstract void move(Direction direction);

}
