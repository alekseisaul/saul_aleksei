package tasks.game;

public class Player extends Unit {
    public Player(int x, int y) {
        super(100, 100, 50);
        this.x = x;
        this.y = y;
        numOfDraw = 1;
    }

    @Override
    void move(Direction direction) {
         x += direction.x;
         y += direction.y;
    }
}
