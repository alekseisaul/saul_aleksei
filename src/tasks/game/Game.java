package tasks.game;

import java.util.Scanner;

public class Game {
    private static int sizeMap = 10;
    private int[][] map = new int[sizeMap][sizeMap];
    private Player player = new Player(1, 4);
//    Goblin goblin = new Goblin(10,1,20);

    private void putObjectOnMap(int x, int y, int obj) {
        map[y][x] = obj;
    }

    private void draw() {
        putObjectOnMap(player.x, player.y, player.numOfDraw);//
//        putObjectOnMap(goblin.x,goblin.y,goblin.numOfDraw);///

        System.out.println("Player's hp = " + player.getHp());
        System.out.println("Player's armor = " + player.getArmor());
        System.out.println("Player's damage = " + player.getDamage());
        for (int[] aMap : map) {
            for (int j = 0; j < aMap.length; j++) {
                System.out.print(aMap[j]);
            }
            System.out.println();
        }
    }

    Game() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j] = 0;
            }
        }
    }

    /***
     * игровой цикл
     */
    void start() {
        Scanner scanner = new Scanner(System.in);
        int action;
        while (true) {
            draw();
            action = scanner.nextInt();
            switch (action) {
                case 2:
                    if (player.canGo(Direction.DOWN, sizeMap)) {
                        player.move(Direction.DOWN);
                        putObjectOnMap(player.x - Direction.DOWN.x, player.y - Direction.DOWN.y, 0);
                    }
                    break;
                case 4:
                    if (player.canGo(Direction.LEFT, sizeMap)) {
                        player.move(Direction.LEFT);
                        putObjectOnMap(player.x - Direction.LEFT.x, player.y - Direction.LEFT.y, 0);
                    }
                    break;
                case 6:
                    if (player.canGo(Direction.RIGHT, sizeMap)) {
                        player.move(Direction.RIGHT);
                        putObjectOnMap(player.x - Direction.RIGHT.x, player.y - Direction.RIGHT.y, 0);
                    }
                    break;
                case 8:
                    if (player.canGo(Direction.UP, sizeMap)) {
                        player.move(Direction.UP);
                        putObjectOnMap(player.x - Direction.UP.x, player.y - Direction.UP.y, 0);
                    }
                    break;
                default:

            }
        }
    }
}

