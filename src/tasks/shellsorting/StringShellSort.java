package tasks.shellsorting;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringShellSort {

    public static void main(String[] args) {
        List<String> sorted;
        File inputFile = new File(File.separator + "Users" + File.separator + "aleksejsaul" + File.separator
                + "IdeaProjects" + File.separator + "JavaMasa" + File.separator + "test.txt");

        File outputFile = new File(File.separator + "Users" + File.separator + "aleksejsaul" + File.separator
                + "IdeaProjects" + File.separator + "JavaMasa" + File.separator + "output.txt");

//        String input = setInputFromConsole();
        String input = serInputFromFile(inputFile);

        sorted = shellSort(input);

//        sorted = sort(input);
        writeToFile(sorted, outputFile);

        System.out.println("Sorted Case Sense" + "\n");

        printSorted(sorted);

        System.out.println();
        System.out.println("-----------------------------" + "\n");
        System.out.println("Sorted Ignore Case" + "\n");

        printSorted(sortIgnoreCase(input));
    }

    public static String setInputFromConsole() {
        String string = "";

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            string = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }

    private static String serInputFromFile(File file) {
        String string = "";

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            string = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return string;
    }

    private static void writeToFile(List<String> list, File outputFile) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile, true))) {
            for (String str : list) {
                bw.write(str + " ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static List<String> sort(String str) {
        List<String> wordList = new ArrayList<>();
        String[] wordArray = str.split(" ");
        Arrays.sort(wordArray);
        Collections.addAll(wordList, wordArray);
        return wordList;
    }

    private static void printSorted(List<String> sorted) {
        for (String tmp : sorted) {
            System.out.println(tmp);
        }
    }

    private static List<String> sortIgnoreCase(String input) {
        String[] words;
        List<String> list = new ArrayList<>();
        words = input.split(" ");

        Collections.addAll(list, words);

        list.sort(String::compareToIgnoreCase);

        return list;
    }

    private static List<String> shellSort(String str) {
        List<String> wordList = new ArrayList<>();
        String[] wordArray = str.split(" ");

        //реализация алгоритма Шелла (вариант Кнутта)

        int h = 1;
        while (h * 3 < wordArray.length) {
            h = h * 3 + 1;
        }

        while (h >= 1) {
            hSort(wordArray, h);
            h = h / 3;
        }

        Collections.addAll(wordList, wordArray);

        return wordList;
    }

    private static void hSort(String[] wordArray, int h) {
        int length = wordArray.length;
        for (int i = h; i < length; i++) {
            for (int j = i; j >= h; j = j - h) {
                if ((wordArray[j].compareToIgnoreCase(wordArray[j - h])) < 0) {
                    swap(wordArray, j, j - h);
                } else {
                    break;
                }
            }
        }
    }

    private static void swap(String[] array, int i, int j) {
        String temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
