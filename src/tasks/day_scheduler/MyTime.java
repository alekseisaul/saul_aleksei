package tasks.day_scheduler;

import java.util.ArrayList;
import java.util.List;

final class MyTime {
    private static volatile MyTime instance;
    private static final List<Integer> BOOKED_MINUTES = new ArrayList<>();
    private static int lastBookedStart;
    private static int lastBookedEnd;

    static MyTime getInstance(){
        if (instance == null) {
            synchronized (MyTime.class) {
                if (instance == null) {
                    instance = new MyTime();
                }
            }
        }
        return instance;
    }

    private MyTime() {}

    private boolean checkTime(int start, int end){
        if(start > end){
            return false;
        }
        //Если лист BOOKED_MINUTES содержит время в заданном диапазоне - интервал недоступен
        for(int i = start; i <= end; i++){
            if(BOOKED_MINUTES.contains(i)){
                return false;
            }
        }
        return true;
    }

    private int convertTimeFromString(String s){
        String[] input = s.split(":");
        return Integer.parseInt(input[0]) * 60 + Integer.parseInt(input[1]);
    }

    private String convertTimeFromInt(int time){
        return String.format("%02d:%02d", time / 60, time % 60);
    }

    boolean bookTime(String startString, String endString){
        int start = convertTimeFromString(startString);
        int end = convertTimeFromString(endString);
        if(checkTime(start, end)){
            for(int i = start; i <= end; i++){
                BOOKED_MINUTES.add(i);
            }
            lastBookedStart = start;
            lastBookedEnd = end;
            return true;
        }
        return false;
    }

    boolean bookTime(String startString, String endString, int duration){
        int start = convertTimeFromString(startString);
        int end = convertTimeFromString(endString);
        for(int i = start; i <= end; i++){
            if(checkTime(i, i + duration)){
                for(int j = i; j <= i + duration; j++){
                    BOOKED_MINUTES.add(j);
                }
                lastBookedStart = i;
                lastBookedEnd = i + duration;
                return true;
            }
        }
        return false;
    }

    boolean releaseTime(){
        BOOKED_MINUTES.clear();
        return true;
    }

    boolean releaseTime(String startString, String endString){
        int start = convertTimeFromString(startString);
        int end = convertTimeFromString(endString);
        if(!checkTime(start, end)){
            for(int i = start; i <= end; i++){
                BOOKED_MINUTES.remove(Integer.valueOf(i));
                return true;
            }
        }
        return false;
    }

    String getLastBookedStart() {
        return convertTimeFromInt(lastBookedStart);
    }

    String getLastBookedEnd() {
        return convertTimeFromInt(lastBookedEnd);
    }
}
