package tasks.day_scheduler;

import java.util.ArrayList;
import java.util.List;

class Day {
    private static final List<Meeting> MEETINGS = new ArrayList<>();
    private static final String ALL_MEETINGS_ARE_DELETED = "All meetings were deleted.\n";
    private static final String THE_TIME_IS_UNAVAILABLE = "The time is unavailable.\n";
    private static final String ERROR_CAN_T_REMOVE_THE_MEETING = "Error!!!\nCan't remove the meeting!\n";
    private static final String ENTER_THE_MEETING_NAME = "Enter the meeting name:";
    private static final String ENTER_THE_MEETING_DESCRIPTION = "Enter the meeting description:";
    private static final String ENTER_THE_MEETING_STARTING_TIME = "Enter the meeting starting time. Format is HH:MM:";
    private static final String ENTER_THE_MEETING_ENDING_TIME = "Enter the meeting ending time:";
    private static final String ENTER_THE_MEETING_DURATION_HOUR = "Enter the meeting duration, hour:";
    private static final String ENTER_THE_NUMBER_OF_MEETING_TO_DELETE = "Enter the number of meeting to delete";
    private static final String WRONG_ARGUMENT = "Wrong argument!";
    private static final String NO_MEETINGS = "There are no meetings!";

    void showAllMeetings(){
        if(!MEETINGS.isEmpty()) {
            System.out.println("\nCurrent schedule:");
            for (int i = 0; i < MEETINGS.size(); i++) {
                System.out.println(i + ":\n" + MEETINGS.get(i).toString());
            }
        } else {
            System.out.println(NO_MEETINGS);
        }
    }

    void addMeeting(){
        String[] params = fillParams();
        try{
            MEETINGS.add(new Meeting(params[0], params[1], params[2], params[3]));
            showAllMeetings();
        } catch (IllegalArgumentException e){
            System.err.println(THE_TIME_IS_UNAVAILABLE);
        }
    }

    void addMeetingWithDuration(){
        String[] params = fillParams();
        System.out.println(ENTER_THE_MEETING_DURATION_HOUR);
        int duration = Integer.parseInt(Util.readFromConsole());
        while(duration > 23 || duration <= 0){
            System.err.println(WRONG_ARGUMENT);
            System.out.println(ENTER_THE_MEETING_DURATION_HOUR);
            duration = Integer.parseInt(Util.readFromConsole());
        }
        try{
            MEETINGS.add(new Meeting(params[0], params[1], params[2], params[3], duration));
            showAllMeetings();
        } catch (IllegalArgumentException e){
            System.err.println(THE_TIME_IS_UNAVAILABLE);
        }
    }

    void removeMeeting(){
        System.out.println(ENTER_THE_NUMBER_OF_MEETING_TO_DELETE);
        int num = Integer.parseInt(Util.readFromConsole());
        Meeting meeting = MEETINGS.get(num);
        if(MyTime.getInstance().releaseTime(meeting.getStart(), meeting.getEnd())) {
            try {
                MEETINGS.remove(num);
                showAllMeetings();
            } catch (ArrayIndexOutOfBoundsException e){
                System.err.println(WRONG_ARGUMENT);
            }
        } else {
            System.err.println(ERROR_CAN_T_REMOVE_THE_MEETING);
        }
    }

    void removeAllMeetings(){
        if(MyTime.getInstance().releaseTime()) {
            MEETINGS.clear();
            System.out.println(ALL_MEETINGS_ARE_DELETED);
        }
    }

    private String[] fillParams(){
        String[] params = new String[4];

        System.out.println(ENTER_THE_MEETING_NAME);
        params[0] = Util.readFromConsole();

        System.out.println(ENTER_THE_MEETING_DESCRIPTION);
        params[1] = Util.readFromConsole();

        System.out.println(ENTER_THE_MEETING_STARTING_TIME);
        params[2] = Util.readFromConsole();
        while(Util.checkArgument(params[2])){
            System.err.println(WRONG_ARGUMENT);
            System.out.println(ENTER_THE_MEETING_STARTING_TIME);
            params[2] = Util.readFromConsole();
        }

        System.out.println(ENTER_THE_MEETING_ENDING_TIME);
        params[3] = Util.readFromConsole();
        while(Util.checkArgument(params[3])){
            System.err.println(WRONG_ARGUMENT);
            System.out.println(ENTER_THE_MEETING_ENDING_TIME);
            params[3] = Util.readFromConsole();
        }
        return params;
    }
}
