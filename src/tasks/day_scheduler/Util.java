package tasks.day_scheduler;

import org.jetbrains.annotations.NotNull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Util {

    private static final String WRONG_ARGUMENT = "Wrong argument!";

    static String readFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    static boolean checkArgument(@NotNull String s) {
        int hour;
        int minute;
        int min = 0;
        int hMax = 23;
        int mMax = 59;

        if(s.matches("\\d+:\\d+")) {
            try {
                String[] tmp = s.split(":");
                hour = Integer.parseInt(tmp[0]);
                minute = Integer.parseInt(tmp[1]);
                if ((hour > hMax || hour < min) ||
                        (minute > mMax || minute < min)) {
                    return true;
                }
            } catch (NumberFormatException e) {
                System.err.println(WRONG_ARGUMENT);
                return true;
            }
        } else {
            return true;
        }
        return false;
    }
}
