package tasks.day_scheduler;

public class DayScheduler {

    private static final String GREETING = "Greeting!\nThis is a Day Planner Util.\n" +
            "You can plan and check your meetings here.\n";
    private static final String MENU = "Press:" +
            "\n0 to observe all existing meetings;" +
            "\n1 to add a meeting in concrete time period;" +
            "\n2 to add a meeting without concrete timing;" +
            "\n3 for remove a meeting;" +
            "\n4 to remove all meetings;" +
            "\n5 to quit.\n";

    public static void main(String[] args) {
        Day day = new Day();
        System.out.println(GREETING);
        while(true) {
            printMenu();

            String consoleInput = Util.readFromConsole();

            switch (consoleInput) {
                case "0":
                    day.showAllMeetings();
                    break;
                case "1":
                    day.addMeeting();
                    break;
                case "2":
                    day.addMeetingWithDuration();
                    break;
                case "3":
                    day.removeMeeting();
                    break;
                case "4":
                    day.removeAllMeetings();
                    break;
                case "5":
                    return;
                default:
                    System.err.println("Wrong command");
                    break;
            }
        }
    }

    private static void printMenu() {
        System.out.println(MENU);
    }
}
