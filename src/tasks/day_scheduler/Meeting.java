package tasks.day_scheduler;

public class Meeting {
    private String name;
    private String description;
    private String start;
    private String end;
    private MyTime time = MyTime.getInstance();

    public Meeting(String name, String description, String start, String end) {
        if (time.bookTime(start, end)) {
            this.name = name;
            this.description = description;
            this.start = time.getLastBookedStart();
            this.end = time.getLastBookedEnd();
        } else throw new IllegalArgumentException();
    }

    public Meeting(String name, String description, String start, String end, int duration) {
        if(time.bookTime(start, end, duration * 60)) {
            this.name = name;
            this.description = description;
            this.start = start;
            this.start = time.getLastBookedStart();
            this.end = time.getLastBookedEnd();
        } else throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return String.format("Meeting name: %s;\nStart time: %s;\nEnd time: %s;\nDescription: %s.\n",
                name,start,end, description);
    }

    String getStart() {
        return start;
    }

    String getEnd() {
        return end;
    }


}
