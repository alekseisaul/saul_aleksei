package cron;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Util {

    private static final String SET_A_DAY_OF_WEEK = "Set a day of week. From 1 to 7, where 1 is Monday, 7 is Sunday. * for any one";
    private static final String SET_A_MONTH = "Set a month. From 1 to 12, where 1 is January, 12 is December. * for any one";
    private static final String SET_A_DAY_OF_MONTH = "Set a day of month. From 1 to 31. * for any one\n\"Be careful!!! It might be a leap year. Also, some month contains less then 31 days!\"";
    private static final String SET_A_HOUR = "Set a hour. From 0 to 23. * for any one";
    private static final String SET_A_MINUTE = "Set a minute. From 0 to 59";
    private static final String SET_AN_EXECUTIVE_FILE = "Set an executive file absolute path";
    private static final String WRONG_ARGUMENT = "Wrong argument!!!";

    private static String parse(@NotNull String arg, String parameter){
        String result;

        if(arg.contains("/")){
            String[] args = arg.split("/");
            return String.format("every %s %s(s) from %s %s(s), ", args[1], parameter, args[0], parameter);
        }

        switch(arg){
            case "?":
                result = String.format("any %s, ", parameter);
                break;
            case "*":
                result = String.format("every %s, ", parameter);
                break;
            default:
                if(parameter.equals("minute")){
                    result = String.format("%s-th(s) %s(s).", arg, parameter);
                } else {
                    result = String.format("%s-th(s) %s(s), ", arg, parameter);
                    break;
                }
        }
        return result;
    }

    @NotNull
    static String parseDate(@NotNull String input){
        //0 14 2 10 * /tmp/test.sh
        StringBuilder sb = new StringBuilder();
        String [] args = input.split(" ");
        sb.append(String.format("The script %s will be run at ", args[args.length -1]));

        for(int i = args.length -2; i >= 0; i--){
            switch (i){
                case 4:
                    sb.append(parse(args[i], "week day"));
                    break;
                case 3:
                    sb.append(parse(args[i], "month"));
                    break;
                case 2:
                    sb.append(parse(args[i], "month day"));
                    break;
                case 1:
                    sb.append(parse(args[i], "hour"));
                    break;
                default:
                    sb.append(parse(args[i], "minute"));
                    break;
            }
        }

        return sb.toString();
    }

    static String formJobString() {
        String dayOfWeek;
        String month;
        String dayOfMonth;
        String hour;
        String minute;
        String path;

        System.out.println(SET_A_DAY_OF_WEEK);
        while(argumentPreCheck(dayOfWeek = readFromConsole(), 1, 7)){
            System.out.println(SET_A_DAY_OF_WEEK);
        }

        System.out.println(SET_A_MONTH);
        while(argumentPreCheck(month = readFromConsole(), 1, 12)){
            System.out.println(SET_A_MONTH);
        }

        System.out.println(SET_A_DAY_OF_MONTH);
        while(checkDayOfMonth(dayOfMonth = readFromConsole())){
            System.out.println(SET_A_DAY_OF_MONTH);
        }

        System.out.println(SET_A_HOUR);
        while(argumentPreCheck(hour = readFromConsole(), 0, 23)){
            System.out.println(SET_A_HOUR);
        }

        System.out.println(SET_A_MINUTE);
        while(argumentPreCheck(minute = readFromConsole(), 0, 59)){
            System.out.println(SET_A_MINUTE);
        }

        System.out.println(SET_AN_EXECUTIVE_FILE);
        path = readFromConsole();

        return String.format("%s %s %s %s %s %s", minute, hour, dayOfMonth, month, dayOfWeek, path);
    }

    private static boolean checkArgument(int min, int max, @NotNull String... args){
        int i;

        for(String tmp : args){
            try{
                i = Integer.parseInt(tmp);
            } catch (NumberFormatException e){
                System.err.println(WRONG_ARGUMENT);
                return true;
            }
            if(!(i <= max && i >= min)){
                System.err.println(WRONG_ARGUMENT);
                return true;
            }
        }

        return false;
    }

    private static boolean argumentPreCheck(@NotNull String arg, int min, int max) {
        if (arg.equals("*") || arg.contains("/") /*|| arg.equals("?")*/) {
            return false;
        }

        if (arg.contains(",")) {
            String[] args = arg.split(",");
            return checkArgument(min, max, args);
        } else if (arg.contains("-")) {
            String[] args = arg.split("-");
            return checkArgument(min, max, args);
        } else {
            return checkArgument(min, max, arg);
        }
    }

    private static boolean checkDayOfMonth(String arg){
        //Check if the year is leap year
        //In case I'd like to make a real checking. Lazy one.
//        System.out.println("Be careful!!! It might be a leap year. Also, some month contains less then 31 days!");
        int min = 1;
        int max = 31;

        return argumentPreCheck(arg, min, max);
    }

    static String readFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return input;
    }
}
