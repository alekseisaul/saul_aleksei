package cron;

public class CrontabMain {

    public static void main(String[] args) {
        JobManager jobManager = new JobManager();

        while (true) {
            jobManager.printExistingJobs();
            System.out.println();

            printMenu();
            String consoleInput = Util.readFromConsole();
            switch (consoleInput) {
                case "0":
                    break;
                case "1":
                    jobManager.addJob();
                    break;
                case "2":
                    jobManager.removeJob();
                    break;
                case "3":
                    return;
                case "4":
                    jobManager.removeAllJobs();
                    break;
                default:
                    System.err.println("Wrong command");
                    break;
            }
        }
    }

    private static void printMenu(){
        System.out.println("Available actions:");
        System.out.println("0. To print all existing jobs");
        System.out.println("1. Add a job;");
        System.out.println("2. Remove a job;");
        System.out.println("3. Exit.");
        System.out.println("4. Crazy: remove all jobs!");
    }
}
