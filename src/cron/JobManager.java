package cron;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static cron.Util.parseDate;

class JobManager {

    private static final String CRONTAB_TMP_MY_CRON = "crontab /tmp/my_cron";
    private static final String CRONTAB_L = "crontab -l";
    private static final String CRONTAB_R = "crontab -r";
    private static final String TMP_MY_CRON = "/tmp/my_cron";

    private final List<String> jobs;
    private final Runtime runtime;
    private InputStream is;

    JobManager() {
        jobs = new ArrayList<>();
        runtime = Runtime.getRuntime();
    }

    void addJob(){
        jobs.add(Util.formJobString());
        writeJobsListToFile();
        try {
            runtime.exec(CRONTAB_TMP_MY_CRON);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void removeJob(){
        System.out.println("Please, enter the number of job to delete.");
        int num = Integer.parseInt(Util.readFromConsole());
        jobs.remove(num);
        writeJobsListToFile();
        try {
            runtime.exec(CRONTAB_R);
            runtime.exec(CRONTAB_TMP_MY_CRON);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void removeAllJobs(){
        jobs.clear();
        try {
            runtime.exec(CRONTAB_R);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void printExistingJobs() {
        fillJobList();

        System.out.println("Existing jobs:");
        if(!jobs.isEmpty()) {
            for (int i = 0; i < jobs.size(); i++) {
                System.out.println("#" + i + ": " + parseDate(jobs.get(i)));
            }
        } else {
            System.err.println("There are no jobs!");
        }
    }

    private void fillJobList(){
        String s;

        try {
            Process process = runtime.exec(CRONTAB_L);
            is = process.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            while(br.ready()){
                s = br.readLine();
                jobs.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeJobsListToFile(){
        File path = new File(TMP_MY_CRON);
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(path))){
            for(String tmp : jobs){
                bw.write(tmp + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
