package rock_paper_scissors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Game {
    private static final String MOVE = "Chose your move:\n0 - for a Paper;\n1 - for a Scissors;\n2 - for a Rock";
    private static final String DO_CONTINUE = "Press 0 to quit the game or press 1 to continue:";
    private static final String WRONG_INPUT = "Wrong input!!!";
    private static final String DRAW = "Draw!";
    private static final String ENTER_PLAYERS_NAME = "Enter players name:";
    private static final String PC_NAME = "Mac";
    private static final List<Integer> MOVES = new ArrayList<>();
    private static final Map<Integer, String> MOVES_NAMES = new HashMap<>();
    private RealPlayer player1; //Human
    private RealPlayer player2; //Mac

    Game(RealPlayer player1, RealPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
        for(int i = 0; i < 3; i++) {
            MOVES.add(i);
        }

        MOVES_NAMES.put(0, "Paper");
        MOVES_NAMES.put(1, "Scissors");
        MOVES_NAMES.put(2, "Rock");
    }

    void play(){
        System.out.println(ENTER_PLAYERS_NAME);
        player1.setName(Util.readFromConsole());
        player2.setName(PC_NAME);

        while(true) {
            try {
                System.out.println(MOVE);
                int move = Integer.parseInt(Util.readFromConsole());
                if(MOVES.contains(move)) {
                    player1.setMove(move);
                    player2.setMove(Util.getRandomNumber());
                    printMoves();
                    findTheWinner();
                    printPoints();
                } else {
                    System.err.println(WRONG_INPUT);
                }
            } catch (NumberFormatException e){
                System.err.println(WRONG_INPUT);
            }

            if(breakTheGame()){
                break;
            }
        }
    }

    private void printMoves() {
        System.out.println(String.format("%s`s move is: %s", player1.getName() , MOVES_NAMES.get(player1.getMove())));
        System.out.println(String.format("%s`s move is: %s", player2.getName() , MOVES_NAMES.get(player2.getMove())));
        System.out.println();

    }

    private void findTheWinner(){
        int a = player1.getMove();
        int b = player2.getMove();
        int diff = a - b;

        if(diff == 1 || diff == -2){
            player1.addPoint();
            System.out.println(String.format("%s Win!", player1.getName()));
        } else if(diff == -1 || diff == 2){
            player2.addPoint();
            System.out.println(String.format("%s Win!", player2.getName()));
        } else {
            System.out.println(DRAW);
        }
        System.out.println();
    }

    private void printPoints(){
        System.out.println(String.format("%s: %d points", player1.getName(), player1.getPoint()));
        System.out.println(String.format("%s: %d points", player2.getName(), player2.getPoint()));
        System.out.println();
    }

    private boolean breakTheGame(){
        System.out.println(DO_CONTINUE);
        try{
            int quit = Integer.parseInt(Util.readFromConsole());
            if(quit == 0) {
                return true;
            } else if(quit == 1){
                return false;
            }
            System.err.println(WRONG_INPUT);
        } catch (NumberFormatException e){
            System.err.println(WRONG_INPUT);
        }
        return breakTheGame();
    }
}
