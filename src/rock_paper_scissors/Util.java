package rock_paper_scissors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Util {

    private static final String WRONG_INPUT = "Wrong input!!!";

    static String readFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            System.err.println(WRONG_INPUT);
        }

        return input;
    }

    static int getRandomNumber(){
        return (int) (Math.random() * 3);
    }
}
