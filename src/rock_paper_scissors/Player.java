package rock_paper_scissors;

//getName возвращает имя игрока
//● getMove возвращает решение игрока на текущий ход
//● addPoint добавляет одно очко игроку, когда он выигрывает
//● getPoints возвращает количество очков игрока

public interface Player {
    String getName();
    int getMove();
    void addPoint();
    int getPoint();

}
