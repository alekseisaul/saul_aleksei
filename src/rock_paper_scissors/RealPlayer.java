package rock_paper_scissors;

public class RealPlayer implements Player {
    private String name;
    private int point;

    void setMove(int move) {
        this.move = move;
    }

    private int move;

    void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getMove() {
        return this.move;
    }

    @Override
    public void addPoint() {
        this.point++;
    }

    @Override
    public int getPoint() {
        return this.point;
    }
}
