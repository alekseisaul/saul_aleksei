package rock_paper_scissors;

import javafx.print.PageLayout;

public class RockPaperScissors {

    public static void main(String[] args) {
        RealPlayer human = new RealPlayer();
        RealPlayer computer = new RealPlayer();
        Game game = new Game(human, computer);
        game.play();
    }
}
