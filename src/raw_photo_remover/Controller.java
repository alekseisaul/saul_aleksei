package raw_photo_remover;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Controller {
    private static final Controller instance = new Controller();
    private static final String CHECKED_ALREADY = "\nThe directory has been checked already.";
    private static final String NEVER_BEEN_CHECKED = "\nThe directory has never been checked.";
    private static View view;
    private static Model model;

    private Controller() {
        view = View.getInstance();
        model = Model.getInstance();
    }

    static Controller getInstance() {
        return Objects.requireNonNullElseGet(instance, Controller::new);
    }

    void setRoot(){
        String path = readFromConsole();
        try {
            model.setRoot(path);
        } catch (rawPhotoRemoverException e) {
            e.printStackTrace();
        }
    }

    String readFromConsole() {
        String input = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            view.printWrongInput();
        }
        return input;
    }

    void getUserInput() {
        String input = readFromConsole();
        switch (input) {
            case "1": //delete all useless raw-files
                model.deleteUselessFiles(false);
                break;
            case "2": //delete all useless raw-files FORCE
                model.deleteUselessFiles(true);
                break;
            case "3": //check if the directory has been checked already
                view.printMessage(model.isDirectoryChecked() ? CHECKED_ALREADY : NEVER_BEEN_CHECKED);
                break;
            case "4":
                view.printRootFolderRequest();
                setRoot();
                break;
            case "5": //Exit
                System.exit(0);
            default:
                view.printWrongInput();
        }
    }
}
