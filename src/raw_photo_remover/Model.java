package raw_photo_remover;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

class Model {
    private static final Model instance = new Model();

    private static final String CHECKED_MARK = "checked_mark";
    private static final String RAW_EXTENSION = ".NEF";
    private static final String PSD_EXTENSION = ".psd";
    private static final String JPG_EXTENSION = ".jpg";
    private static final String XMP_EXTENSION = ".xmp";
    private static final String EXCEPTION_NOT_A_FOLDER = "Input path is not a folder!";
    private static final String ALREADY_REORGANIZED = "The folder has been already reorganized.";
    private static final String WILL_BE_DELETED = "Next files will be deleted:";
    private static final String CONFIRM_PRESS_Y = "To confirm press y";

    private static File root;
    private static Set<File> rawToDelete;
    private static Controller controller = Controller.getInstance();
    private static View view = View.getInstance();

    private Model() {
    }

    static Model getInstance() {
        return Objects.requireNonNullElseGet(instance, Model::new);
    }

    void deleteUselessFiles(boolean force) {
        if (isDirectoryChecked() && !force) {
            System.out.println(ALREADY_REORGANIZED);
        } else {
            if (checkIfRawNeedToBeDeleted()) {
                view.printMessage(WILL_BE_DELETED);
                view.printFileSet(rawToDelete);
                view.printMessage(CONFIRM_PRESS_Y);
                if (controller.readFromConsole().toLowerCase().equals("y")) {
                    removeFiles();
                }
            }
            touchMarkFile();
        }
    }

    private void touchMarkFile() {
        File mark = new File(root, CHECKED_MARK);
        try(BufferedWriter bwr = new BufferedWriter(new FileWriter(mark))){
            bwr.write(new Date().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeFiles() {
        StringBuffer sb = new StringBuffer();
        for (File raw : rawToDelete) {
            sb.append("file: ").append(raw.getName())
                    .append(" deleted: ").append(raw.delete())
                    .append(" timestamp: ").append(new Date()).append("\n");
        }
        view.writeLog(sb);
    }

    boolean isDirectoryChecked() {
        return new File(root, CHECKED_MARK).exists();
    }

    private File getFolder(File[] files, String folderName) {
        File folder;
        for (File tmp : files) {
            if (tmp.getName().toLowerCase().equals(folderName) && tmp.exists()) {
                view.printMessage(String.format("Folder %s has already exist", folderName));
                return tmp;
            }
        }
        folder = new File(root, folderName);
        try {
            folder.mkdir();
            view.printMessage(String.format("Folder %s created", folderName));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return folder;
    }

    private boolean checkIfRawNeedToBeDeleted() {
        File[] allFiles = root.listFiles();
            Set<File> rawFiles = new HashSet<>(
                    FileUtils.listFiles(root, new SuffixFileFilter(RAW_EXTENSION),
                            TrueFileFilter.INSTANCE));
            Set<File> psdFiles = new HashSet<>(FileUtils.listFiles(getFolder(allFiles, "psd"),
                    new SuffixFileFilter(PSD_EXTENSION), TrueFileFilter.INSTANCE));
            Set<File> jpgFiles = new HashSet<>(
                    FileUtils.listFiles(getFolder(allFiles, "jpg"),
                            new SuffixFileFilter(JPG_EXTENSION), TrueFileFilter.INSTANCE));
            Set<File> xmpFiles = new HashSet<>(
                    FileUtils.listFiles(root, new SuffixFileFilter(XMP_EXTENSION),
                            TrueFileFilter.INSTANCE));
            rawToDelete = new HashSet<>();
            Set<String> psdFilesNames = new HashSet<>();
            Set<String> jpgFilesNames = new HashSet<>();
            Set<String> xmpFileNames = new HashSet<>();

            for (File psd : psdFiles) {
                psdFilesNames.add(psd.getName());
            }
            for (File jpg : jpgFiles) {
                jpgFilesNames.add(jpg.getName());
            }
            for (File xmp : xmpFiles) {
                xmpFileNames.add(xmp.getName());
            }
            for (File tmp : rawFiles) {
                if (!(psdFilesNames.contains(tmp.getName().replace(RAW_EXTENSION, PSD_EXTENSION)) ||
                        jpgFilesNames.contains(tmp.getName().replace(RAW_EXTENSION, JPG_EXTENSION)) ||
                        xmpFileNames.contains(tmp.getName().replace(RAW_EXTENSION, XMP_EXTENSION)))) {
                    rawToDelete.add(tmp);
                }
            }
        return rawFiles.size() > 0;
    }

    File getRoot() {
        return root;
    }

    void setRoot(String path) throws rawPhotoRemoverException {
        root = new File(path);
        if (!root.isDirectory()) {
            throw new rawPhotoRemoverException(EXCEPTION_NOT_A_FOLDER);
        }
    }
}