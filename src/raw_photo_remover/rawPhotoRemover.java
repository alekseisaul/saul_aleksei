package raw_photo_remover;

public class rawPhotoRemover {
    private static Controller controller = Controller.getInstance();
    private static View view = View.getInstance();

    public static void main(String[] args) {
        view.printGreeting();
        view.printRootFolderRequest();
        controller.setRoot();
        while(true) {
            view.printMenu();
            controller.getUserInput();
        }
    }
}
