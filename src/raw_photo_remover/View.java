package raw_photo_remover;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

class View {
    private static final String WRONG_INPUT = "Wrong input!!!";
    private static final String GREETING = "This is a raw file remover Util." +
            " The util cleans up selected folder from unused Raw-photos;";
    private static final String INPUT_REQUEST = "Input a path to a folder you'd like to process";
    private static final String MENU = "\nPress a key for:\n1 - delete all useless raw-files;" +
            "\n2 - delete all useless raw-files FORCE;" +
            "\n3 - check if the directory has been checked already;" +
            "\n4 - select another directory;" +
            "\n5 - exit.";

    private static final View instance = new View();
    private static final String ASK_FOR_RAW_TYPE = "Set raw-file extension (NEF, RAW, etc.";
    private static Model model = Model.getInstance();

    private View() {

    }

    static View getInstance() {
        return Objects.requireNonNullElseGet(instance, View::new);
    }

    void printGreeting() {
        System.out.println(GREETING);
    }

    void printRootFolderRequest() {
        System.out.println(INPUT_REQUEST);
    }

    void printWrongInput() {
        System.err.println(WRONG_INPUT);
    }

    void printMenu() {
        System.out.println(MENU);
    }

    void printFileSet(Set<File> set) {
        for (File tmp : set) {
            System.out.println(tmp.getName());
        }
    }

    void askForRawType() {
        System.out.println(ASK_FOR_RAW_TYPE);
    }

    void printMessage(String message) {
        System.out.println(message);
    }

    void writeLog(StringBuffer sb) {
        File log = new File(model.getRoot(), "log.txt");

        try(BufferedWriter bwr = new BufferedWriter(new FileWriter(log, true))){
            bwr.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
