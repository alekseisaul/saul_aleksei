package simple_bash_interpreter;

import javafx.util.Pair;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import static simple_bash_interpreter.SimpleBashInterpreter.NO_SUCH_DIRECTORY;
import static simple_bash_interpreter.SimpleBashInterpreter.currentFolder;
import static simple_bash_interpreter.SimpleBashInterpreter.pwd;

public class Util {
    private static final String WRONG_INPUT = "Wrong input!!!";

    static String readFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            System.err.println(WRONG_INPUT);
        }

        return input;
    }

    static String[] readArrayFromConsole(){
        String input = "";

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return input.split(" ");
    }

    static void checkIfDirectoryExists(){
        if (currentFolder.exists()) {
            pwd();
        } else {
            System.err.println(NO_SUCH_DIRECTORY);
        }
    }

    static void setParentFolder(){
        if(!isRoot()) {
                currentFolder = currentFolder.getParentFile();
                pwd();
        } else {
            System.err.println(WRONG_INPUT);
        }
    }

    private static boolean isRoot() {
        return currentFolder.getAbsolutePath().equals("/");
    }

    static void setSubFolder(@NotNull String path){
        currentFolder = new File(currentFolder.getAbsolutePath() + path.substring(1));
        Util.checkIfDirectoryExists();
    }
}
