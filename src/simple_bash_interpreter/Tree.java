package simple_bash_interpreter;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Objects;

class Tree implements MyPrintableInterface{
    private static final String TABULATOR = "    ";
    private static final String LINE = "|---";
    private static final String FILEMARK = "(f)";
    private static File currentFolder;

    Tree(File file) {
        currentFolder = file;
    }

    private static void printDelimiter(int treeLevel){
        for(int i = 0; i < treeLevel; i++){
            System.out.print(TABULATOR);
        }
        System.out.print(LINE);
    }

    private static void printFile(@NotNull File file, int treeLevel){
        printDelimiter(treeLevel);
        System.out.println(file.getName() + FILEMARK);
    }

    private static void printFolder(@NotNull File file, int treeLevel){
        printDelimiter(treeLevel);
        System.out.println(file.getName());
    }

     public void print(){
        printUsingTreeLevels(currentFolder, 0);
    }

    private static void printUsingTreeLevels(File currentFolder, int treeLevel){
        try {
            for (File tmp : Objects.requireNonNull(currentFolder.listFiles())) {
                if (tmp.isFile()) {
                    printFile(tmp, treeLevel);
                } else if (tmp.isDirectory()) {
                    printFolder(tmp, treeLevel);
                    printUsingTreeLevels(tmp, treeLevel + 1);
                }
            }
        } catch (NullPointerException e){
        }
    }
}
