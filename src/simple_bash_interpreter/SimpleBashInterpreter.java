package simple_bash_interpreter;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;

class SimpleBashInterpreter {
    private static final String NO_ARGUMENT = "No argument";
    private static final String WRONG_COMMAND = "Wrong command";
    private static final String INPUT_COMMAND = "Please, input command...\n";
    private static final String CURRENT_DIRECTORY_IS = "Current directory is: ";
    static final String NO_SUCH_DIRECTORY = "No such directory";
    private static final String GREETING = "\nSimple_bash_interpreter util - provides cd/ls/pwd commands.\n" +
            "\"For exit print \"Exit\" or \"exit\"\"";
    static File currentFolder = new File("/");

    public static void main(String[] args) {
        String[] input;
        String command;
        String arg;

        printGreeting();
        while (true) {
            input = Util.readArrayFromConsole();
            command = input[0];
            switch (command) {
                case "ls":
                    ls(input);
                    break;
                case "pwd":
                    pwd();
                    break;
                case "cd":
                    try{
                        arg = input[1];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.err.println(NO_ARGUMENT);
                        break;
                    }
                    cd(arg);
                    break;
                case "grep":
                    try {
                        grep(input[2], input[1]);
                    } catch (ArrayIndexOutOfBoundsException e){
                        System.err.println(NO_ARGUMENT);
                        break;
                    }
                    break;
                case "tree":
                    printTree();
                    break;
                case "Exit":
                    return;
                case "exit":
                    return;
                default:
                    System.err.println(WRONG_COMMAND);
                    break;
            }
        }
    }

    private static void ls(String[] input){
        for(File file : Objects.requireNonNull(currentFolder.listFiles())){
            if(input.length > 1 && input[1].equals("-a")){
                if(!file.getName().startsWith(".")){
                    System.out.println(file.getName());
                }
            } else {
                System.out.println(file.getName());
            }
        }
    }

    static void pwd(){
        System.out.print(CURRENT_DIRECTORY_IS);
        System.out.println(currentFolder.getAbsolutePath());
    }

    private static void cd(@NotNull String path){
        //Обратка относительных ссылок
        String[] parsed;
        if(path.contains("/") && path.length() > 1) {
            parsed = path.split("/");
            //переход в родительский каталог
            switch (parsed[0]) {
                case "..":
                    Util.setParentFolder();
                    //переход в подкаталог
                    break;
                case ".":
                    Util.setSubFolder(path);
                    //для абсолютных ссылок
                    break;
                default:
                    currentFolder = new File(path);
                    Util.checkIfDirectoryExists();
                    break;
            }
            //переход в родительский каталог
        } else if(path.equals("..")) {
            Util.setParentFolder();
            //переход в подкаталог без указания /
        } else if(path.contains("/") && path.length() == 1){
            currentFolder = new File("/");
            Util.checkIfDirectoryExists();
        } else {
            path = currentFolder.getAbsolutePath() + "/" + path;
            currentFolder = new File(path);
            Util.checkIfDirectoryExists();
        }
    }

    private static void printGreeting(){
        System.out.println(GREETING);
        System.out.println();
        pwd();
        System.out.println();
        System.out.println(INPUT_COMMAND);
    }

    private static void grep(String word, String path){
        Date start = new Date();
        Path file = Paths.get(path);
        if(Files.isSymbolicLink(file)){
            try {
                file = Files.readSymbolicLink(file);
                String str = "/" + file.toString();
                System.out.println(str);
                file = Paths.get(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Files.walkFileTree(file, new MyFieVisitor(word));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Date finish = new Date();
        System.out.println(finish.getTime() - start.getTime());
    }

    private static void printTree(){
        Tree tree = new Tree(currentFolder);
        tree.print();
    }
}
