package simple_bash_interpreter;


import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class MyFieVisitor extends SimpleFileVisitor<Path> {
    private String word;

    MyFieVisitor(String word) {
        this.word = word;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(attrs.isSymbolicLink()){
            file = Files.readSymbolicLink(file);
            String tmp = file.toFile().getAbsolutePath();
            file = Paths.get(File.separator + tmp);
        }

        if(attrs.isRegularFile()){
            findTheWord(file.toFile(), word);
        }
        return FileVisitResult.CONTINUE;
    }

    private static void findTheWord(File file, String word) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while (br.ready()) {
                String str = br.readLine();
                if (str.contains(word)) {
                    System.out.println(word + ": " + file);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
